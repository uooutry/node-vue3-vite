import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path"
import WindiCSS from 'vite-plugin-windicss'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    WindiCSS(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      // eslint-disable-next-line no-undef
      "~":path.resolve(__dirname,"src")
    },
    server:{
      proxy:{
        '/api':{
          target:'http://127.0.0.1:3005',
          changeOrigin:true,
          rewrite: (path) => path.replace(/^\/api/,'')
        }
      }
    }
  }
})


import requset from './request'
import { throttle } from 'lodash';

const token = localStorage.getItem('token')
export const login =(data)=>{
    return requset({
        url:'/user/login',
        method:"POST",
        data
    })
}


export const reguser = (data)=>{
    return requset({
        url:'/user/reguser',
        method:"POST",
        data
    })
}

export const userList = (data) =>{
    return requset({
        url:'/user/userinfo',
        method:"get",
        params: data
    })
}



export const getuserList = async () =>{
    return requset({
        url:'/user',
        method:"get",
       
    })
}



export const delectUser = (data) =>{
    return requset({
        url:'/user/deletuser',
        method:"post",
        data
    })
}

export const upUser = (data) =>{
    return requset({
        url:'/user',
        method:"POST",
        data
    })
}

export const upuserInfo = (data) =>{
 
    return requset({
        url:'/user/userinfo',
        method:"post",
        data
    })
}


export const upload = (data) =>{
    return requset({
        url:'user/upload',
        method:"POST",
      
        data:data
    })
}
export const cancel = (data) =>{
    return requset({
        url:'/delect/uplads',
        method:"post",
        data
    })
}


export const upavatar = (data)=>{
    return requset({
        url:'/user/update/avatar',
        method:"POST",
      
        data:data
    })
}

export const getImage = (data) =>{
  
    return requset({
        url:'/uploads',
        method:"get",
        params:data
    })
}

export const getAddresses = (data) =>{
    return requset({
        url:'/user/addresses',
        method:"get",
        params:data
    })
}


export const addResses = (data)=>{
    return requset({
        url:'/user/addresses',
        method:"post",
        data
    })
}
export const upaddResses = (data)=>{
    return requset({
        url:'/user/upaddresses',
        method:"post",
        data
    })
}
export const upPwd = (data)=>{
   
    return requset({
        url:'/user/userpwd',
        method:"post",
        data
    })
}

export const addBanner = (data)=>{
   
    return requset({
        url:'/banner',
        method:"post",
        data,
        headers: {
            Authorization: `Bearer ${token}` // 添加token到请求头中
          }
    })
}

export const getBanner = ()=>{
   
    return requset({
        url:'/banner',
        method:"get",
        
    })
}

export const detBanner = (data)=>{
   
    return requset({
        url:'/delct/banner',
        method:"post",
        data
    })
}
export const upBanner = (data)=>{
   
    return requset({
        url:'/updata/banner',
        method:"post",
        data
    })
}


export const getarticles =() => {
    return requset({
        url:'/articles',
        method:"get",
        
    })
}

export const addarticles = (data) =>{
   
    return requset({
        url:'/add/articles',
        method:"post",
        data
    })
}


export const getProducts = () =>{
    return requset({
        url:'/products',
        method:"get",
       
    })
}

export const addProducts = (data) =>{
    return requset({
        url:'/products',
        method:"post",
        data
    })
}


export const upProducts = (data) =>{
    return requset({
        url:'/update/products',
        method:"post",
        data
    })
}


export const delectProducts = (data) =>{
    return requset({
        url:'/delct/products',
        method:"post",
        data
    })
}


export const getCategory = () =>{
    return requset({
        url:'/category',
        method:"get",
    })
}


export const getProCategory = () =>{
    return requset({
        url:'/get/productscategory',
        method:"get",
       
    })
}
export const musicCategory = () =>{
    return requset({
        url:'/get/musicscategory',
        method:"get",
       
    })
}

export const videoCategory = () =>{
    return requset({
        url:'/get/videosategory',
        method:"get",
       
    })
}



export const addCategory = (data) =>{
    return requset({
        url:'/category',
        method:"post",
        data
    })
}



export const getOrder = (data) =>{
    return requset({
        url:'/get/orders',
        method:"get",
        params:data
    })
}

export const getadminOrder = (data) =>{
    return requset({
        url:'/get/adminorders',
        method:"get",
        params:data
    })
}

export const Delivergoods = (data)=>{
    return requset({
        url:'/push/Delivergoods',
        method:"post",
        data
    })
}



export const addmusics = (data)=>{
    return requset({
        url:'/add/musics',
        method:"post",
        data
    })
}

export const getmusics = () =>{
    return requset({
        url:'/get/musics',
        method:'get',
      
    })
}

export const getusermusics = (data) =>{
    return requset({
        url:'/getuser/musics',
        method:'get',
        params:data
    })
}



export const delectmusics = (data) =>{
    return requset({
        url:'/delct/musics',
        method:'post',
        data
    })
}


export const updataMusics = (data) =>{
    return requset({
        url:'/updata/musics',
        method:'post',
        data
    })
}


export const audio = (data) =>{
  
    return requset({
        url:'/user/upload',
        method:"POST",
        data:data
    })
}


export const addvideos = (data) =>{
  
    return requset({
        url:'/add/videos',
        method:"POST",
        data:data
    })
}

export const getadminvideos = (data) =>{
  
    return requset({
        url:'/get/adminvideos',
        method:"get",
        params:data
    })
}

export const delectvideos = (data) =>{
  
    return requset({
        url:'/delect/adminvideos',
        method:"post",
        data:data
    })
}


export const upvideos = (data) =>{
  
    return requset({
        url:'/updat/videos',
        method:"post",
        data:data
    })
}


export const set = (data) => {
    const url = `/set/${data.action}`; // 使用动态字段拼接 URL
    
    // eslint-disable-next-line no-undef
    return requset({
      url: url,
      method: 'get',
      params: data.params
    });
  };


  export const $Gets = (data)=>{

    return requset({
        url: "/get/userauth",
        method: 'get',
        params: data
      });

  }
  

  


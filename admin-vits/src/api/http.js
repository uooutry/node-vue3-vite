import requset from './request'



  export const $post = (data) =>{
    const url = `${data.action}`; // 使用动态字段拼接 URL
    
    return requset({
      url: url,
      method: 'post',
      data:data.params
    });
  }


  export const $get = async (data) => {
    const url = `${data.action}`; // 使用动态字段拼接 URL
    const token = localStorage.getItem('token');
    return await requset({
      url: url,
      method: 'get',
      params: data.params,
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }


  
export const $set = (data) => {
    const url = `${data.action}`; // 使用动态字段拼接 URL
    
    // eslint-disable-next-line no-undef
    return requset({
      url: url,
      method: 'get',
      params: data.params
    });
  };

import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import './assets/global.css';

import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import locale from "element-plus/es/locale/lang/zh-cn"

const app = createApp(App)

app.use(createPinia())
app.use(router).use(ElementPlus,{locale})
import 'virtual:windi.css'

app.mount('#app')

import { createRouter, createWebHistory } from 'vue-router'
import Index from '../views/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Index,
      children:[
        {
          path: '/home',
          name: 'home',
          title:"首页",
          component: () => import('../views/home/home.vue'),
          meta:{
            title:"首页",
            index: 1 
            
          }
        },
        {
          path: '/admin',
          name: 'admin',
          title: '用户管理',
          component: () => import('../views/user/admin.vue'),
          meta:{
            title:"超级管理员",
            index: 2,
            required:0
            
          }
          
        },
        {
          path: '/vip',
          name: 'vip',
          title: '用户管理',
          component: () => import('../views/user/vip.vue'),
          meta:{
            title:"会员用户",
            index: 3,
            required:1
          }
        },
        {
          path: '/ordinary_user',
          name: 'ordinary_user',
          title: '用户管理',
          component: () => import('../views/user/ordinary_user.vue'),
          meta:{
            title:"普通用户",
            index: 4,
            required:2
          }
        },
        {
          path: '/user_info',
          name: 'user_info',
          title: '用户管理',
          component: () => import('~/components/userinfo/userinfo.vue'),
          meta:{
            title:"个人中心",
         
          
          }
        },
        {
          path: '/up_passowrd',
          name: 'passowrd',
          title: '用户管理',
          component: () => import('~/components/userinfo/uppassword.vue'),
          meta:{
            title:"修改密码",
         
          
          }
        },
        {
          path: '/banner',
          name: 'banner',
          title: '轮播图管理',
          component: () => import('../views/banner/banner.vue'),
          meta:{
            title:"轮播图",
            requiresAuth: true 
          }
        },
        {
          path: '/articles',
          name: 'articles',
          title: '轮播图管理',
          component: () => import('../views/articles/articles.vue'),
          meta:{
            title:"文章",
          }
        },
        {
          path: '/category',
          name: 'category',
          title: '分类管理',
          component: () => import('../views/category/category.vue'),
          meta:{
            title:"分类",
          }
        },
        {
          path: '/products',
          name: 'products',
          title: '商品管理',
          component: () => import('../views/products/products.vue'),
          meta:{
            title:"商品",
          }
        },
        {
          path: '/order',
          name: 'order',
          title: '订单管理',
          component: () => import('../views/order/order.vue'),
          meta:{
            title:"订单",
          }
        },
        {
          path: '/orderadmin',
          name: 'orderadmin',
          title: '店家订单管理',
          component: () => import('../views/orderadmin/adminorder.vue'),
          meta:{
            title:"订单",
          }
        },
        {
          path: '/music',
          name: 'music',
          title: '音乐管理列表',
          component: () => import('../views/music/music.vue'),
          meta:{
            title:"音乐",
          }
        },
        {
          path: '/usermusic',
          name: 'usermusic',
          title: '音乐列表',
          component: () => import('../views/music/usermusic.vue'),
          meta:{
            title:"音乐",
          }
        },
        {
          path: '/adminvideo',
          name: 'adminvideo',
          title: '视频管理列表',
          component: () => import('../views/video/adminvideo.vue'),
          meta:{
            title:"视频管理员",
          }
        },
        {
          path: '/uservideo',
          name: 'uservideo',
          title: '视频列表',
          component: () => import('../views/video/uservideo.vue'),
          meta:{
            title:"视频用户",
          }
        },

        {
          path: '/episodes',
          name: 'episodes',
          title: '视频集数',
          component: () => import('../views/video/episodes.vue'),
          meta:{
            title:"视频集数",
          }
        },
        {
          path: '/comments',
          name: 'comments',
          title: '音乐评论',
          component: () => import('../views/comments/comments.vue'),
          meta:{
            title:"音乐评论",
          }
        },
        {
          path: '/videocomments',
          name: 'videocomments',
          title: '视频评论',
          component: () => import('../views/comments/videocomments.vue'),
          meta:{
            title:"视频评论",
          }
        },
      ]
    },



    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/reguser',
      name: 'Reguser',
      component: () => import('../views/Reguser.vue')
    }
  ]
})

export default router

import { ref, computed } from 'vue'
import { defineStore } from 'pinia';

export const useAccountStore = defineStore('data', {
  state: () => ({
    account: '',
    created_at: '',
    email: '',
    id: '',
    name: '',
    password: '',
    permission: '',
    phone: '',
    status:'',
    updated_at: '',
  }),
  mutations: {
    setAccountData(data) {
      for (const key in data) {
        this[key] = data[key];
      }
    },
  },
});

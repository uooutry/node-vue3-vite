import {getCurrentInstance,reactive,ref} from 'vue'

export const getFileName = () => {
    const filePath = getCurrentInstance().type.__file; // 获取组件文件路径
    const fileNameWithExtension = filePath.split('/').pop(); // 提取文件名（包含后缀名）
    const fileName = fileNameWithExtension.split('.')[0]; // 去除后缀名
    return fileName;
  }



export const getProductsWithImageUrls = async (val) =>{
    try {
      const response = await val;
      const tableDatas = response.map(obj => {
        const imgUrls = obj.image_urls && typeof obj.image_urls === 'string' ? obj.image_urls.split(',') : [];
        return {
          ...obj,
          image_urls: imgUrls
        };
      });
      return tableDatas;
    } catch (error) {
      console.error('Error retrieving product data:', error);
      return [];
    }
  }
  
  export function updateCategoryData(data) {
    // 在这里处理接收到的数据
    // 可以将数据存储到 auth.js 中的 category 对象中
    const category = reactive({
        data
    })
    return category
  }


  export let convertImageUrlsToArrays = (list) => {
    for (var i = 0; i < list.length; i++) {
      // 将image_urls字符串转换为数组
      list[i].image_urls = list[i].image_urls.split(",");
    }
  
    return list;
  }

 
export  const getAudioDuration = async (file) => {
    const audioContext = new AudioContext();
    const fileReader = new FileReader();
    
    const duration = ref(null);
    let tiem = ref(null)
    fileReader.onload = () => {
      audioContext.decodeAudioData(fileReader.result)
        .then(decodedData => {
          duration.value = decodedData.duration;
          tiem.value = formatTime(duration.value )
    
        })
        .catch(error => {
          console.log("解码音频文件出错：" + error.message);
        });
    };
  
    fileReader.readAsArrayBuffer(file);
    function formatTime(value) {
      const minutes = Math.floor(value / 60);
    const seconds = Math.floor(value % 60);
  
    return `${minutes}:${padZero(seconds)}`;
  
     
    }
  
    function padZero(num) {
      return num.toString().padStart(2, '0');
    }
   
    return tiem;
  };


  export  const getCurrentDate = () => {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    
    const formattedDate = `${year}-${month}-${day}`; // 格式化日期为 'YYYY-MM-DD' 格式
    
    return formattedDate;
  }


  export const  convertTimeString = (timeString) => {
    const dateObj = new Date(timeString);
    const year = dateObj.getFullYear();
    const month = dateObj.getMonth() + 1; // 月份从 0 开始，所以需要加 1
    const day = dateObj.getDate();
  
    // 返回正常格式的日期，例如 "2023-10-18"
    return `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`;
  }



  export const debounce = (func, delay) => {
    let timeoutId;
  
    return function (...args) {
      clearTimeout(timeoutId);
  
      timeoutId = setTimeout(() => {
        func.apply(this, args);
        console.log(...args);
      }, delay);
    };
  };
import { createRouter, createWebHistory } from 'vue-router'
import Home from '~/views/home.vue'
import ofo from '~/views/404.vue'




const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        children:[
          {
            path: '/userinfo',
            name: 'userinfo',
            title:"个人中心",
            component: () => import('../views/userinfo/index.vue'),
            meta:{
              title:"个人中心",
              index: 1 
              
            },
            children:[
              {
                path: '/userinfo/homepage',
                name: 'homepage',
                title:"主页",
                component: () => import('../components/userinfoc/Homepage.vue'),
                meta:{
                  title:"主页",
                  index: 1 
                  
                }
              },
              {
                path: '/userinfo/works',
                name: 'works',
                title:"作品",
                component: () => import('../components/userinfoc/works.vue'),
                meta:{
                  title:"作品",
                  index: 2
                  
                }
              },
              {
                path: '/userinfo/collect/video',
                name: 'video',
                title:"视频",
                component: () => import('../components/collectList/video.vue'),
                meta:{
                  title:"视频收藏",
                  index: 6
                  
                }
              },
              {
                path: '/userinfo/collect/musics',
                name: 'usermusics',
                title:"音乐",
                component: () => import('../components/collectList/musics.vue'),
                meta:{
                  title:"音乐收藏",
                  index: 6
                  
                }
              },
              {
                path: '/userinfo/collect/products',
                name: 'collectproducts',
                title:"商品收藏",
                component: () => import('../components/collectList/products.vue'),
                meta:{
                  title:"商品收藏",
                  index: 7
                  
                }
              },
              {
                path: '/userinfo/order',
                name: 'order',
                title:"订单",
                component: () => import('../components/userinfoc/order.vue'),
                meta:{
                  title:"订单",
                  index: 8
                  
                }
              },

              {
                path: '/userinfo/user_edit',
                name: 'user_edit',
                title:"编辑信息",
                component: () => import('../components/userinfoc/user_edit.vue'),
                meta:{
                  title:"编辑信息",
                  index: 5
                  
                }
              },
              {
                path: '/userinfo/passworde',
                name: 'passworde',
                title:"修改密码",
                component: () => import('../components/cpmms/cuserpassword.vue'),
                meta:{
                  title:"修改密码",
                  index: 6
                  
                }
              },

              
            ]


          },
          {
            path: '/index',
            name: 'index',
            title:"首页",
            component: () => import('../views/index/index.vue'),
            meta:{
              title:"首页",
              index: 1 
              
            },
          },
          {
            path: '/index/viedeo',
            name: 'viedeo',
            title:"视频",
            component: () => import('../components/cpmms/cvideo.vue'),
            meta:{
              title:"视频",
              index: 1 
              
            }
          },
          
          {
            path: '/musics',
            name: 'musics',
            title:"音乐",
            component: () => import('../views/musics/index.vue'),
            meta:{
              title:"音乐",
              index: 2
              
            }
          },
          {
            path: '/musics/mv',
            name: 'musicsmv',
            title:"音乐MV",
            component: () => import('../components/cpmms/cmusics.vue'),
            meta:{
              title:"音乐VM",
              index: 3
              
            }
          },
          {
            path: '/musics/Publishmusics',
            name: 'Publishmusics',
            title:"音乐发布",
            component: () => import('../views/musics/Publishmusics.vue'),
            meta:{
              title:"音乐发布",
              index: 4
              
            }
          },
          {
            path: '/video/Publishvideo',
            name: 'Publishvideo',
            title:"视频发布",
            component: () => import('../views/vidoe/Publishvideo.vue'),
            meta:{
              title:"视频发布",
              index: 5
              
            }
          },
          {
            path: '/video/addepisodevudei',
            name: 'addepisodevudei',
            title:"视频发布",
            component: () => import('../views/vidoe/addepisodevudei.vue'),
            meta:{
              title:"视频发布",
              index: 5
              
            },
       
          },

          {
            path: '/Aboutus',
            name: 'Aboutus',
            title:"关于我们",
            component: () => import('../views/Aboutus/index.vue'),
            meta:{
              title:"关于我们",
              index: 8
              
            },
        
          },
          {
            path: '/articles',
            name: 'articles',
            title:"文章",
            component: () => import('../views/articles/index.vue'),
            meta:{
              title:"文章",
              index: 9
              
            },
           
          },
          {
            path: '/products',
            name: 'products',
            title:"藏品购物",
            component: () => import('../views/products/index.vue'),
            meta:{
              title:"藏品购物",
              index: 10
              
            },
           
          },
          {
            path: '/index/cproducts',
            name: 'cproducts',
            title:"藏品详情",
            component: () => import('../components/cpmms/cproducts.vue'),
            meta:{
              title:"藏品详情",
              index: 11
              
            },
           
          },
        ]
        
    },
    {
      path: '/login',
      name: 'login',
      title:"登录",
      component: () => import('../views/Login.vue'),
      meta:{
        title:"登录",
        index: 1 
        
      }
    },
    {
      path: '/reguser',
      name: 'reguser',
      title:"注册",
      component: () => import('../views/Reguser.vue'),
      meta:{
        title:"注册",
        index: 1 
        
      }
    },
    {
      path: '/404',
      name: '404',
      component: ofo
    }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
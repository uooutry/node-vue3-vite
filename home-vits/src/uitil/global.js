import {getCurrentInstance,reactive,ref} from 'vue'
import {$post ,$get} from '../api/page'
import { throttle } from "lodash";


export const getFileName = () => {
    const filePath = getCurrentInstance().type.__file; // 获取组件文件路径
    const fileNameWithExtension = filePath.split('/').pop(); // 提取文件名（包含后缀名）
    const fileName = fileNameWithExtension.split('.')[0]; // 去除后缀名
    return fileName;
  }



export const getProductsWithImageUrls = async (val) =>{
    try {
      const response = await val;
      const tableDatas = response.map(obj => {
        const imgUrls = obj.image_urls && typeof obj.image_urls === 'string' ? obj.image_urls.split(',') : [];
        return {
          ...obj,
          image_urls: imgUrls
        };
      });
      return tableDatas;
    } catch (error) {
      console.error('Error retrieving product data:', error);
      return [];
    }
  }
  
  export function updateCategoryData(data) {
    // 在这里处理接收到的数据
    // 可以将数据存储到 auth.js 中的 category 对象中
    const category = reactive({
        data
    })
    return category
  }


  export let convertImageUrlsToArrays = (list) => {
    for (var i = 0; i < list.length; i++) {
      // 将image_urls字符串转换为数组
      list[i].image_urls = list[i].image_urls.split(",");
    }
  
    return list;
  }

 
export  const getAudioDuration = async (file) => {
    const audioContext = new AudioContext();
    const fileReader = new FileReader();
    
    const duration = ref(null);
    let tiem = ref(null)
    fileReader.onload = () => {
      audioContext.decodeAudioData(fileReader.result)
        .then(decodedData => {
          duration.value = decodedData.duration;
          tiem.value = formatTime(duration.value )
    
        })
        .catch(error => {
          console.log("解码音频文件出错：" + error.message);
        });
    };
  
    fileReader.readAsArrayBuffer(file);
    function formatTime(value) {
      const minutes = Math.floor(value / 60);
    const seconds = Math.floor(value % 60);
  
    return `${minutes}:${padZero(seconds)}`;
  
     
    }
  
    function padZero(num) {
      return num.toString().padStart(2, '0');
    }
   
    return tiem;
  };


  export  const getCurrentDate = () => {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    
    const formattedDate = `${year}-${month}-${day}`; // 格式化日期为 'YYYY-MM-DD' 格式
    
    return formattedDate;
  }


  export const  convertTimeString = (timeString) => {
    const dateObj = new Date(timeString);
    const year = dateObj.getFullYear();
    const month = dateObj.getMonth() + 1; // 月份从 0 开始，所以需要加 1
    const day = dateObj.getDate();
  
    // 返回正常格式的日期，例如 "2023-10-18"
    return `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`;
  }



  export const debounce = (func, delay) => {
    let timeoutId;
  
    return function (...args) {
      clearTimeout(timeoutId);
  
      timeoutId = setTimeout(() => {
        func.apply(this, args);
        console.log(...args);
      }, delay);
    };
  };



  export const getuserinfo = async (user) => {
    const userinfo = {
      action:'/user/userinfo',
      params: {
        user_id : user
      }
    }
    let userinfoData = reactive(
      {}
    )
   await $get(userinfo).then((res)=>{
       userinfoData = res
   })
    return userinfoData
  }

  export const getaddesses =async (user) => {
    const userinfo = {
      action:'/user/addresses',
      params: {
        user_id : user
      }
    }
    let userinfoData = reactive(
      {}
    )
   await $get(userinfo).then((res)=>{
       userinfoData = res
   })
    return userinfoData
  }


  //转化时间格式
  export let convertDatesToNormalFormat = async (data) => {
    const normalizedData = [];
  
    for (let i = 0; i < data.length; i++) {
      const obj = data[i];
  
      // 使用 await 等待异步操作完成
      const normalizedDate = await new Promise((resolve) => {
        setTimeout(() => {
          resolve(new Date(obj.created_at).toLocaleString());
        }, 0);
      });
  
      const normalizedObj = { ...obj, created_at: normalizedDate };
      normalizedData.push(normalizedObj);
    }
  
    return normalizedData;
  };
  
  // 获取四份最新上线
  export let getLatestData = async (data) => {
    const normalizedData = await convertDatesToNormalFormat(data);
    
    let latestData = [];
    let latestDates = [];
  
    // 找到最新日期且 mv_path 不为空
    for (let i = 0; i < normalizedData.length; i++) {
      const obj = normalizedData[i];
      const currentDate = new Date(obj.created_at);
  
      // 添加条件检查：mv_path 不为空
      if (obj.mv_path !== null && obj.mv_path !== undefined && obj.mv_path !== "") {
        if (latestDates.length < 4) {
          latestData.push(obj);
          latestDates.push(currentDate);
        } else {
          let earliestIndex = 0;
          let earliestDate = latestDates[0];
  
          // 找到最早日期
          for (let j = 1; j < latestDates.length; j++) {
            if (currentDate < earliestDate) {
              earliestIndex = j;
              earliestDate = latestDates[j];
            }
          }
  
          // 如果当前日期比最早日期晚，则替换最早日期和数据
          if (currentDate > earliestDate) {
            latestData[earliestIndex] = obj;
            latestDates[earliestIndex] = currentDate;
          }
        }
      }
    }
  
    return latestData;
  };

  export let getVidoetData = async (data) => {
    const normalizedData = await convertDatesToNormalFormat(data);
    
    let latestData = [];
    let latestDates = [];
  
    // 找到最新日期且 mv_path 不为空
    for (let i = 0; i < normalizedData.length; i++) {
      const obj = normalizedData[i];
      const currentDate = new Date(obj.created_at);
  
      // 添加条件检查：mv_path 不为空
   
        if (latestDates.length < 4) {
          latestData.push(obj);
          latestDates.push(currentDate);
        } else {
          let earliestIndex = 0;
          let earliestDate = latestDates[0];
  
          // 找到最早日期
          for (let j = 1; j < latestDates.length; j++) {
            if (currentDate < earliestDate) {
              earliestIndex = j;
              earliestDate = latestDates[j];
            }
          }
  
          // 如果当前日期比最早日期晚，则替换最早日期和数据
          if (currentDate > earliestDate) {
            latestData[earliestIndex] = obj;
            latestDates[earliestIndex] = currentDate;
          }
        }
     
    }
  
    return latestData;
  };
  
  
  export const findAllObjectsByCategory = (arr, category) => {
    const foundObjects = [];
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].category === category) {
        foundObjects.push(arr[i]);
      }
    }
    return foundObjects;
  };


  export  const formatDate = (arr) =>{
    const result = [];
    for (let i = 0; i < arr.length; i++) {
      const obj = arr[i];
      const { upload_date, ...rest } = obj;
      const date = new Date(upload_date);
      const formattedDate = date.toLocaleString();
      result.push({ upload_date: formattedDate, ...rest });
    }
    return result;
  }

  //推荐相同商品
  export const getLimitedData = (data, category, limit)=> {
    const result = [];
    let count = 0;
    
    for (let i = 0; i < data.length; i++) {
        if (data[i].category === category) {
            result.push(data[i]);
            count++;
            
            if (count === limit) {
                break;
            }
        }
    }
    
    return result;
}

//订单排序

export function findItems(array) {
  const sortedArray = array.sort((a, b) => {
    if (a.pending_shipment === 0 && b.pending_shipment !== 0) {
      return -1; // a排在b前面
    }
    if (a.pending_shipment !== 0 && b.pending_shipment === 0) {
      return 1; // b排在a前面
    }

    const aTime = new Date(a.orders_time).getTime();
    const bTime = new Date(b.orders_time).getTime();

    return bTime - aTime;
  });
  
  return sortedArray;
}
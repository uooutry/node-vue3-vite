import { ElMessage } from 'element-plus'

// 消失提示
import { debounce } from 'lodash';

export const toast = debounce((message, type = 'success', dangerouslyUseHTMLString = false) => {
  ElMessage({
    message: message,
    type,
    dangerouslyUseHTMLString,
    duration: 2000
  });
}, 1000); 


export const loginSta =  function userIsLoggedIn() {
  const token = localStorage.getItem('token');
  
  return !!token; // 假设有 token 就表示已登录
}

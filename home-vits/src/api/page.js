
import requset from './request'
import { throttle } from 'lodash';



export const set = (data) => {
  const token = localStorage.getItem('token')
    const url = `/set/${data.action}`; // 使用动态字段拼接 URL
    return requset({
      url: url,
      method: 'get',
      params: data.params,
      headers: {
        Authorization: `Bearer ${token}` // 添加token到请求头中
      }
    });
  };

  export const $post = (data) =>{
    const url = `${data.action}`; // 使用动态字段拼接 URL
    const token = localStorage.getItem('token')
    return requset({
      url: url,
      method: 'post',
      data:data.params,
      headers: {
        Authorization: `Bearer ${token}` // 添加token到请求头中
      }
    });
  }


  export const $get = async (data) => {
    const url = `${data.action}`; // 使用动态字段拼接 URL
    const token = localStorage.getItem('token')
    return await requset({
      url: url,
      method: 'get',
      params: data.params,
      headers: {
        Authorization: `Bearer ${token}` // 添加token到请求头中
      }
    });
  }

  
  export const upload = (data) =>{
    return requset({
        url:'user/upload',
        method:"POST",
      
        data:data
    })
}
  


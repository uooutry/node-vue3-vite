

import axios from 'axios'

  
import { toast } from "~/uitil/tool.js"

const service = axios.create({
  baseURL:'http://127.0.0.1:3005/api',
 
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded', // 第一个 headers
  
    'X-Custom-Header': 'Custom Value' // 第三个 headers
    
  },
})


// 添加请求拦截器
axios.interceptors.request.use(function (config) {
// 在发送请求之前做些什么

let token = localStorage.getItem('token')

config.headers['Authorization'] = 'Bearer ' + token;
return config;
}, function (error) {
// 对请求错误做些什么
return Promise.reject(error);
});

// 响应拦截器
service.interceptors.response.use((response)=>{
  
 
  const {data} = response.data

  if (response.data.status === 0 || response.status === 0) {
   
    toast(response.data.message)
    return data
  }else{
   
  return Promise.reject(new Error(response.data.message))
  }
  // error=>{
  //   error.response(&& ELMessage.error(response.data.message))
  //   return  Promise.reject(new Error(response.data.message))
  // }
})


export default service
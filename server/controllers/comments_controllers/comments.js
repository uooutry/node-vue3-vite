
const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const {formatDate,TimeFormatter} = require('../../utils/schema')

exports.Comments = (req, res) => {
  // 从请求参数中获取字段和商品ID
  const productId = req.body.product_id;
  const videoId = req.body.video_id;
  const musicId = req.body.music_id;

  let query;
  let params;

  // 根据有值字段选择查询条件和参数
  if (productId) {
    query = "SELECT id, user_id, username, product_id, comment_text, comment_table, likes, positive_ratings, negative_ratings, created_at, avatar_path FROM comments WHERE product_id = ? and deleted = 0";
    params = [productId];
  } else if (videoId) {
    query = "SELECT id, user_id, username, product_id, comment_text, comment_table, likes, positive_ratings, negative_ratings, created_at, avatar_path FROM comments WHERE video_id = ? and deleted = 0";
    params = [videoId];
  } else if (musicId) {
    query = "SELECT id, user_id, username, product_id, comment_text, comment_table, likes, positive_ratings, negative_ratings, created_at, avatar_path FROM comments WHERE music_id = ? and deleted = 0";
    params = [musicId];
  } else {
    // 如果没有提供有效的查询字段，则返回错误信息
    return res.status(400).json({ error: 'No valid query field provided' });
  }

  db.query(query, params, (error, results) => {
    if (error) {
      return res.status(500).json({ error: 'An error occurred while retrieving data' });
    }
    
    const formatter = new TimeFormatter(results, "created_at"); // 第一个参数为数组，第二个参数开始为多个时间字段名称
    const formattedArr = formatter.format();
    
    res.status(200).json({
      status: 0,
      message: '获取成功',
      data:formattedArr
    });
  });
};

exports.AddComments = (req, res) => {
  const { user_id, username, product_id, video_id, music_id, comment_text, comment_table, positive_ratings, negative_ratings, avatar_path, likes } = req.body;

  let idFieldName;
  let idValue;

  // 根据有值字段选择对应的ID字段和值
  if (product_id) {
    idFieldName = 'product_id';
    idValue = product_id;
  } else if (video_id) {
    idFieldName = 'video_id';
    idValue = video_id;
  } else if (music_id) {
    idFieldName = 'music_id';
    idValue = music_id;
  } else {
    // 如果没有提供有效的ID字段，则返回错误信息
    return res.status(400).json({ error: 'No valid ID field provided' });
  }

  const sql = `INSERT INTO comments (user_id, username, ${idFieldName}, comment_text, comment_table, positive_ratings, negative_ratings, avatar_path, likes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;

  db.query(sql, [user_id, username, idValue, comment_text, comment_table, positive_ratings, negative_ratings, avatar_path, likes], (error, results) => {
    if (error) {
      console.log(error);
      return res.send({
        status:1,
        message:'请登录在评论'
      })

    }
    let fonsDate = formatDate(results)
    res.send({
      status: 0,
      message: '评论成功',
      data: fonsDate
    });
  });
};

  exports.incrementLikes = (req, res) => {
    const { postId, userid } = req.body;
    const sqlSelect = "SELECT likes FROM comments WHERE id = ?";
    const sqlUpdate = "UPDATE comments SET likes = ? WHERE id = ?";
    
    // 查询用户是否已点赞
    const sqlCheckLiked = "SELECT * FROM user_likes WHERE user_id = ? AND comment_id = ?";
  
    db.query(sqlCheckLiked, [userid, postId], (error, results) => {
      if (error) {
        console.log(error);
        return res.status(500).json({ error: 'An error occurred while retrieving data' });
      }
  
      if (results.length > 0) {
        // 用户已点赞，返回提示信息
        return res.status(200).json({
          status: 1,
          message: '您已经点过赞了',
        });
      }
  
      // 用户未点赞，执行点赞操作
      db.query(sqlSelect, [postId], (error, results) => {
        if (error) {
          console.log(error);
          return res.status(500).json({ error: 'An error occurred while retrieving data' });
        }
  
        const likes = results[0].likes + 1;
  
        db.query(sqlUpdate, [likes, postId], (error, result) => {
          if (error) {
            console.log(error);
            return res.status(500).json({ error: 'An error occurred while updating data' });
          }
  
          // 记录用户已点赞
          const sqlInsertLiked = "INSERT INTO user_likes (user_id, comment_id) VALUES (?, ?)";
          db.query(sqlInsertLiked, [userid, postId], (error, result) => {
            if (error) {
              console.log(error);
              return res.status(500).json({ error: 'An error occurred while inserting data' });
            }
  
            res.status(200).json({
              status: 0,
              message: '点赞成功',
              data: result
            });
          });
        });
      });
    });
  }


  exports.getCommentsAll = (req, res) => {
    // 从前端请求中获取用户id和音乐id
    const { userId, musicId,videoid } = req.query;
  
   
    if (videoid) {
      // 查询所有和用户id匹配的视频数据
    const queryMusic = `SELECT * FROM videos WHERE user_id = ${userId} `;
    db.query(queryMusic, (err, musicResults) => {
      if (err) {
        console.error('Error querying music table:', err);
        return res.status(500).json({ error: 'Failed to fetch music data' });
      }
  
      // 在评论表中查找对应视频的评论数据
      const queryComments = `SELECT * FROM comments WHERE video_id = ${videoid} and deleted = 0`;
      db.query(queryComments, (err, commentResults) => {
        if (err) {
          console.error('Error querying comments table:', err);
          return res.status(500).json({ error: 'Failed to fetch comments data' });
        }
      
        // 将评论数据返回给前端
        res.send({
          status:0,
          message:'获取成功',
          data:commentResults
        })
      });
    });
    }else{
 // 查询所有和用户id匹配的音乐数据
 const queryMusic = `SELECT * FROM musics WHERE user_id = ${userId} `;
 db.query(queryMusic, (err, musicResults) => {
   if (err) {
     console.error('Error querying music table:', err);
     return res.status(500).json({ error: 'Failed to fetch music data' });
   }

   // 在评论表中查找对应音乐的评论数据
   const queryComments = `SELECT * FROM comments WHERE music_id = ${musicId} and deleted = 0`;

   db.query(queryComments, (err, commentResults) => {
     if (err) {
       console.error('Error querying comments table:', err);
       return res.status(500).json({ error: 'Failed to fetch comments data' });
     }
   
     // 获取所有评论的 ID
     const commentIds = commentResults.map(comment => comment.id);
   
     // 查询 replies 表中符合评论 ID 条件的数据
     const queryReplies = `SELECT * FROM replies WHERE comment_id IN (${commentIds.join(',')})`;
     db.query(queryReplies, (err, replyResults) => {
       if (err) {
         console.error('Error querying replies table:', err);
         return res.status(500).json({ error: 'Failed to fetch replies data' });
       }
   
       // 将 replies 数据按照 comment_id 进行分组
       const groupedReplies = replyResults.reduce((acc, reply) => {
         const commentId = reply.comment_id;
         if (!acc[commentId]) {
           acc[commentId] = [];
         }
         acc[commentId].push(reply);
         return acc;
       }, {});
   
       // 将 replies 数据添加到对应的评论中
       const commentsWithReplies = commentResults.map(comment => ({
         ...comment,
         replies: groupedReplies[comment.id] || []
       }));
   
       // 将评论数据返回给前端，包括 replies 数据
       res.send({
         status: 0,
         message: '获取成功',
         data: commentsWithReplies
       });
     });
   });
 });

    }
  };


  exports.deleteComment = (req, res) => {
    const {commentId } = req.body;
  
    const updateQuery = `UPDATE comments SET deleted = 1 WHERE id = ${commentId}`;
    
    db.query(updateQuery, (err, result) => {
      if (err) {
        console.error('Error updating comment record:', err);
        return res.status(500).json({ error: 'Failed to delete comment' });
      }
      
      res.send({
        status:0,
        message:'删除成功',
        data:result
      });
    });
  };




exports.addReplies = (req, res) => {
  const comment_id = req.body.comment_id;
  const reply_text = req.body.reply_text;
  const user_id = req.body.user_id;
  
  // 插入回复信息到数据库
  const sql = `INSERT INTO replies (comment_id, reply_text, user_id) VALUES (${comment_id}, '${reply_text}', ${user_id})`;

  db.query(sql, (err, result) => {
    if (err) {
      console.error('插入回复信息失败', err);
      res.status(500).json({ message: '插入回复信息失败' });
    } else {
     
      let fonsDate = formatDate(result)
      res.send({
        status: 0,
        message: '回复成功',
        data:fonsDate
      })
    }
  });
}


exports.getReplies = (req, res) => {
  const comment_id = req.query.comment_id;
  
  // 插入回复信息到数据库
  const sql = `select * from replies where comment_id = '${comment_id}'`;

  db.query(sql, (err, result) => {
    if (err) {
      console.error('获取回复信息失败', err);
      res.status(500).json({ message: '获取回复信息失败' });
    } else {
     
      
      if (result==[]) {
        // 如果回复信息为空，则不返回数据
        res.send({
          status: 0,
          message: '暂无数据',
         
        });
      } else {
        let fonsDate = formatDate(result);
        res.send({
          status: 0,
          message: '获取成功',
          data: fonsDate
        });
      }
    }
  });
}
const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const {convertDateFormat} = require('../../utils/schema')
const {TimeFormatter} = require('../../utils/schema')


exports.addvideos = (req, res) => {
  
        // 从前端传来的请求中获取视频信息
   
        const { user_id, username, title, url, upload_date,duration,cover_url,category } = req.body;
       
      
        // 将视频信息和视频信息插入数据库
        const sql = 'INSERT INTO videos (user_id, username, title, url, upload_date,duration,cover_url,category) VALUES (?, ?, ?, ?, ?, ?,?,?)';
        const values = [user_id, username, title, url, upload_date,duration,cover_url,category];
      
        db.query(sql, values, (err, result) => {
          if (err) {
            console.error('视频数据库添加失败:', err);
            return res.status(500).json({ error: '视频数据库添加失败' });
          }
          return res.send({
            status:0,
            message:'添加成功',
            data:result
          })
        });
      };

      exports.getadminvideos = (req, res) => {
        let {user_id} = req.query
        // 从前端传来的请求中获取视频信息 
        if (user_id) {
            const sql = `select * from videos where is_deleted = 0 and user_id = ${user_id}`;
       
      
                db.query(sql, (err, result) => {
                if (err) {
                    console.error('视频获取失败:', err);
                    return res.status(500).json({ error: '视频获取失败' });
                }
                const formatter = new TimeFormatter(result, "created_at", 'updated_at','upload_date'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
                const formattedArr = formatter.format();
                return res.send({
                    status:0,
                    message:'获取成功',
                    data:formattedArr
                })
                });
        }else{

                const sql = 'select * from videos where is_deleted = 0';
                    
                    
                db.query(sql, (err, result) => {
                if (err) {
                    console.error('视频获取失败:', err);
                    return res.status(500).json({ error: '视频获取失败' });
                }

                const formatter = new TimeFormatter(result, "created_at", 'updated_at','upload_date'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
                const formattedArr = formatter.format();
                return res.send({
                    status:0,
                    message:'获取成功',
                    data:formattedArr
                })
                });
        }
       
        

      };




      
  


      exports.delectvideos = (req, res) => {
        // 从前端传来的请求中获取视频信息
        let { id ,episode} = req.body

        if (episode) {
          const sql = `UPDATE episodes SET is_deleted = 1 where id = ${id}`;
          db.query(sql, (err, result) => {
            if (err) {
              console.error('视频信息读取数据库失败:', err);
              return res.status(500).json({ error: '视频信息读取数据库失败' });
            }
            return res.send({
              status:0,
              message:'删除成功',
              data:result
            })
          });
        }else{
            // 将视频信息和视频信息插入数据库
            const sql = 'UPDATE  videos SET is_deleted = 1 where id = ?';
                  
            db.query(sql,id , (err, result) => {
              if (err) {
                console.error('视频信息读取数据库失败:', err);
                return res.status(500).json({ error: '视频信息读取数据库失败' });
              }
              return res.send({
                status:0,
                message:'删除成功',
                data:result
              })
            });
        }
       

      
      };



      exports.uptvideos = (req, res) => {
        // 从前端传来的请求中获取视频信息
        let {id ,user_id, username, title, url, upload_date,duration,cover_url } = req.body
        // 将视频信息和视频信息插入数据库
        const sql = `UPDATE  videos SET title = ?, url=? , upload_date=?,duration=?,cover_url=? where id = ${id} and user_id = ${user_id}`
        const newDateFormat =  convertDateFormat(upload_date)
        db.query(sql,[title, url, newDateFormat,duration,cover_url], (err, result) => {
          if (err) {
            console.error('视频信息读取数据库失败:', err);
            return res.send({
                status:0,
                message:'无权修改',
                data:result
              });
          }
          return res.send({
            status:0,
            message:'修改成功',
            data:result
          })
        });
      };



      exports.getObject = (req,res)=>{
        let {id} = req.query
        const sql = `select * from videos where id = ${id}`
        db.query(sql, (err, result) => {
          if (err) {
            console.error('视频信息读取数据库失败:', err);
            return res.status(500).json({ error: '视频信息读取数据库失败' });
          }
          return res.send({
            status:0,
            message:'获取成功',
            data:result
          })
        });
      }


      
exports.setVideos = (req, res) => {
    let { id , username, title,season,episode} = req.query;
    if (episode) {
          // 构建 SQL 查询语句
        let sql = "SELECT * FROM episodes WHERE 1=1";
        if (id) {
          sql += ` AND id = '${id}'`;
        }
        if (season) {
          sql += ` AND season = '${season}'`;
        }
        if (episode) {
          sql += ` AND episode = '${episode}'`;
        }
        if ( title) {
          sql += ` AND episode = '${episode}'`;
        }
        db.query(sql, (err, results) => {
          if (err) {
            // 处理错误情况
            console.error("Error executing SQL query:", err);
            res.status(500).json({ error: "Internal Server Error" });
          } else {
            // 将查询结果返回给前端
            res.send({
              status:0,
              message:"查询成功",
              data: results
            })
          }
        });

    }else{
       // 构建 SQL 查询语句
    let sql = "SELECT * FROM videos WHERE 1=1";
    if (id) {
      sql += ` AND id = '${id}'`;
    }
    if (title) {
      sql += ` AND title = '${title}'`;
    }
    if (username) {
      sql += ` AND username = '${username}'`;
    }
   
  
    // 执行查询语句
   db.query(sql, (err, results) => {
      if (err) {
        // 处理错误情况
        console.error("Error executing SQL query:", err);
        res.status(500).json({ error: "Internal Server Error" });
      } else {
        // 将查询结果返回给前端
        res.send({
          status:0,
          message:"查询成功",
          data: results
        })
      }
    });
    }
   
  };

  exports.likes = (req, res) => {
    let { id, userid } = req.body;
  
    // Check if user has already liked the video
    const checkLikedSql = `SELECT * FROM user_likes WHERE user_id = ${userid} AND video_id = ${id}`;
  
    db.query(checkLikedSql, (err, result) => {
      if (err) {
        console.error('查询点赞记录失败:', err);
        return res.status(500).json({ error: '查询点赞记录失败' });
      }
  
      if (result.length > 0) {
        // User has already liked the video, return error response
        return res.send({
          status: 1,
          message: '已点赞过该视频',
        });
      } else {
        // Insert user like record into user_likes table
        const insertLikeSql = `INSERT INTO user_likes (user_id, video_id) VALUES (${userid}, ${id})`;
  
        db.query(insertLikeSql, (err, result) => {
          if (err) {
            console.error('点赞失败:', err);
            return res.status(500).json({ error: '点赞失败' });
          }
  
          // Update likes count in videos table
          const updateLikesSql = `UPDATE videos SET likes = likes + 1 WHERE id = ${id}`;
  
          db.query(updateLikesSql, (err, result) => {
            if (err) {
              console.error('更新视频点赞数失败:', err);
              return res.status(500).json({ error: '更新视频点赞数失败' });
            }
  
            return res.send({
              status: 0,
              message: '点赞成功',
            });
          });
        });
      }
    });
  };


  exports.subTableslikes = (req, res) => {
    let { id, userid } = req.body;
  
    // Check if user has already liked the video
    const checkLikedSql = `SELECT * FROM user_likes WHERE user_id = ${userid} AND video_subtable_id = ${id}`;
  
    db.query(checkLikedSql, (err, result) => {
      if (err) {
        console.error('查询点赞记录失败:', err);
        return res.status(500).json({ error: '查询点赞记录失败' });
      }
  
      if (result.length > 0) {
        // User has already liked the video, return error response
        return res.send({
          status: 1,
          message: '已点赞过该视频',
        });
      } else {
        // Insert user like record into user_likes table
        const insertLikeSql = `INSERT INTO user_likes (user_id, video_subtable_id) VALUES (${userid}, ${id})`;
  
        db.query(insertLikeSql, (err, result) => {
          if (err) {
            console.error('点赞失败:', err);
            return res.status(500).json({ error: '点赞失败' });
          }
  
          // Update likes count in videos table
          const updateLikesSql = `UPDATE episodes SET likes = likes + 1 WHERE id = ${id}`;
  
          db.query(updateLikesSql, (err, result) => {
            if (err) {
              console.error('更新视频点赞数失败:', err);
              return res.status(500).json({ error: '更新视频点赞数失败' });
            }
  
            return res.send({
              status: 0,
              message: '点赞成功',
            });
          });
        });
      }
    });
  };

  exports.favorites = (req, res) => {
    const { userid, videoid } = req.body;
  
    // 查询是否已存在收藏记录
    const checkExistSql = 'SELECT COUNT(*) AS count FROM video_collections WHERE user_id = ? AND video_id = ?';
    const checkExistValues = [userid, videoid];
  
    db.query(checkExistSql, checkExistValues, (error, results) => {
      if (error) {
        console.error('查询收藏记录时出错：', error);
        res.status(500).json({ error: '查询收藏记录时出错' });
      } else {
        const count = results[0].count;
        if (count > 0) {
          // 已存在收藏记录
          res.status(400).json({ message: '不能重复收藏' });
        } else {
          // 插入收藏记录
          const insertSql = 'INSERT INTO video_collections (user_id, video_id) VALUES (?, ?)';
          const insertValues = [userid, videoid];
  
          db.query(insertSql, insertValues, (error, results) => {
            if (error) {
              console.error('插入收藏记录时出错：', error);
              res.status(500).json({ error: '插入收藏记录时出错' });
            } else {
             
  
              // 更新视频收藏数
              const updateSql = 'UPDATE videos SET favorites = favorites + 1 WHERE id = ?';
              const updateValues = [videoid];
  
              db.query(updateSql, updateValues, (error, results) => {
                if (error) {
                  console.error('更新视频收藏数时出错：', error);
                  res.status(500).json({ error: '更新视频收藏数时出错' });
                } else {
               
                  res.status(200).json({ message: '收藏成功' });
                }
              });
            }
          });
        }
      }
    });
  };


// 获取用户收藏的视频信息
exports.getUserFavorites = (req, res) => {
  const userId = req.query.userid; // 用户ID

  // 查询用户收藏的视频信息
  const sql = 'SELECT * FROM video_collections WHERE user_id = ?';
  const values = [userId];

 db.query(sql, values, (error, results) => {
    if (error) {
      console.error('查询用户收藏的视频信息时出错：', error);
      res.status(500).json({ error: '查询用户收藏的视频信息时出错' });
    } else {
      if (results.length === 0) {
        // 收藏表中没有对应的记录
        res.status(404).json({ message: '用户没有收藏任何视频' });
      } else {
        const videoIds = results.map((result) => result.video_id);
        const videosQuery = 'SELECT * FROM videos WHERE id IN (?)';
        const videosValues = [videoIds];

       db.query(videosQuery, videosValues, (error, videos) => {
          if (error) {
            console.error('查询视频信息时出错：', error);
            res.status(500).json({ error: '查询视频信息时出错' });
          } else {
            
            res.send({
              status:0,
              message:'查询成功',
              data:videos
            })
          }
        });
      }
    }
  });
};


exports.addplay = (req,res)=>{
  let {id,episodes} = req.body
  
  if (episodes) {
    const sql = `update episodes set play_count = play_count + 1 where id = ${id}`
    db.query(sql, (err, result) => {
      if (err) {
        console.error('视频信息读取数据库失败:', err);
        return res.send({
            status:0,
            message:'无权修改',
            data:result
          });
      }
      return res.send({
        status:0,
        message:'添加成功'
      })
    });
  }else{
    const sql = `update videos set play_count = play_count + 1 where id = ${id}`
    db.query(sql, (err, result) => {
      if (err) {
        console.error('视频信息读取数据库失败:', err);
        return res.send({
            status:0,
            message:'无权修改',
            data:result
          });
      }
      return res.send({
        status:0
      })
    });
  }
}



exports.getAll = (req, res) => {
  // 获取要查询的关键字
  const keyword = req.query.keyword;

  // 存储查询结果
  let queryResults = [];

  // 获取数据库连接
 

  // 查询表，匹配 title、artist和user_name字段，按与关键字接近程度排序
  const query = `
    SELECT *,
      CASE WHEN title LIKE '%${keyword}%' THEN 1 ELSE 0 END AS title_match
      
    FROM videos
    WHERE title LIKE '%${keyword}%'
      
    ORDER BY 
      CASE WHEN title LIKE '%${keyword}%' THEN 1 ELSE 0 END DESC
    `;
  
  // 执行查询
  db.query(query, (error, results, fields) => {
    if (error) {
      throw error;
    }

    // 将查询结果存入结果集
    queryResults = results;

    // 发送结果给客户端进行响应
    res.send({
      status: 0,
      message: '查询成功',
      data: queryResults,
    });
  });
};




exports.addEpisodes = (req, res) => {
  const { video_id, author, title, season, episode, release_date, video_url, image_url } = req.body;

  // 假设您已经创建了一个名为 "connection" 的 MySQL 连接
  const query = `INSERT INTO episodes (video_id, author, title, season, episode, release_date, video_url, image_url)
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
  const values = [video_id, author, title, season, episode, release_date, video_url, image_url];

  db.query(query, values, (error, results) => {
    if (error) {
      res.status(500).json({ message: 'Error adding episode.', error: error });
    } else {
      res.status(200).json({ message: 'Episode added successfully.' });
    }
  });
};



exports.addEpisodes = (req, res) => {
  const { video_id, author, title, season, episode,  video_url, image_url } = req.body;

  // 假设您已经创建了一个名为 "connection" 的 MySQL 连接
  const query = `INSERT INTO episodes (video_id, author, title, season, episode,  video_url, image_url)
                 VALUES (?, ?, ?, ?,  ?, ?, ?)`;
  const values = [video_id, author, title, season, episode,  video_url, image_url];

  db.query(query, values, (error, results) => {
    if (error) {
      res.status(500).json({ message: 'Error adding episode.', error: error });
    } else {
      res.status(200).json({ message: '添加成功' });
    }
  });
};




exports.getEpisodes = (req, res) => {
  const { video_id } = req.query;

  // 假设您已经创建了一个名为 "connection" 的 MySQL 连接
  const query = `SELECT *
  FROM episodes
  WHERE video_id = ${video_id} and is_deleted = 0`;
 

  db.query(query, (error, results) => {
    if (error) {
      res.status(500).json({ message: 'Error adding episode.', error: error });
    } else {
      res.send(
        {
          status: 0,
          message: '查询成功',
          data: results
        }
      )
    }
  });
};


exports.getEpisodesObj = (req, res) => {
  const { video_id } = req.query;

  // 假设您已经创建了一个名为 "connection" 的 MySQL 连接
  const query = `SELECT *
  FROM episodes
  WHERE id = ${video_id} and is_deleted = 0`;
 

  db.query(query, (error, results) => {
    if (error) {
      res.status(500).json({ message: 'Error adding episode.', error: error });
    } else {
      res.send(
        {
          status: 0,
          message: '查询成功',
          data: results
        }
      )
    }
  });
};


exports.getallviePlayCount = (req, res) => {
  const { user_id } = req.query;
  const videosQuery = `SELECT id, category, play_count FROM videos WHERE user_id = ${user_id}`;

  db.query(videosQuery, (error, videosResults) => {
    if (error) {
      res.status(500).json({ message: 'Error getting videos.', error: error });
    } else {
      const videoData = videosResults.map(videoResult => {
        const { id, category, play_count } = videoResult;
        return {
          id,
          category,
          play_count
        };
      });
      
      let totalPlayCount = 0;

      videoData.forEach((video, index) => {
        const episodesQuery = `SELECT play_count FROM episodes WHERE video_id = ${video.id}`;
        db.query(episodesQuery, (error, episodesResults) => {
          if (error) {
            res.status(500).json({ message: 'Error getting episodes.', error: error });
          } else {
            const episodePlayCounts = episodesResults.map(episodeResult => episodeResult.play_count);
            const episodeTotalPlayCount = episodePlayCounts.reduce((total, count) => total + count, 0);
      

            videoData[index].play_count += episodeTotalPlayCount;
            
            if (index === videoData.length - 1) {
              // This is the last video, send the response
            
              res.send({
                status: 0,
                message: '查询成功',
                data: videoData
                 
              
              });
            }
          }
        });
      });
    }
  });
  };

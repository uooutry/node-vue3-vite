const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const {formatDate} = require('../../utils/schema')

exports.getMessage = (req, res) => {
    const user_id = req.query.user_id;
    if (!user_id) {
      return res.status(400).json({ error: 'Missing required parameter: user_id' });
    }
  
    db.query(`SELECT DISTINCT video_id FROM comments`, (error, results) => {
      if (error) {
        console.error(`Error querying database: ${error}`);
        return res.status(500).json({ error: 'Internal Server Error' });
      }
  
      const videoIds = results.map((result) => result.video_id);
  
      db.query(`SELECT * FROM videos WHERE id IN (?) AND user_id = ?`, [videoIds, user_id], (error, results) => {
        if (error) {
          console.error(`Error querying database: ${error}`);
          return res.status(500).json({ error: 'Internal Server Error' });
        }
  
        const videoIds = results.map((result) => result.id);
  
        db.query(`SELECT * FROM comments WHERE video_id IN (?)`, [videoIds], (error, results) => {
          if (error) {
            console.error(`Error querying database: ${error}`);
            return res.status(500).json({ error: '查询失败' });
          }
          let fonsDate = formatDate(results)
          return res.send({
            status:0,
            data:fonsDate,
            message:'查询成功'
          });
        });
      });
    });
  };
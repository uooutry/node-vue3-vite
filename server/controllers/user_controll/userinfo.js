const db = require('../../config/db_config')
const { promisify } = require('util');
const query = promisify(db.query).bind(db);
const fs = require('fs');

exports.getUserInfo = async (req, res) => {
  const sql = `SELECT ui.user_id, ui.avatar_data, ui.image_path, ui.text_content, ui.age, ui.gender, u.id ,u.name, u.account, u.email, u.phone, u.created_at, u.permission, u.status  FROM user_info ui JOIN users u ON ui.user_id = u.id WHERE ui.user_id = ?`;
  
  try {
    const userInfo = await query(sql, [req.query.user_id]);
  
    
    if (userInfo.length !== 1) {
      throw new Error('获取用户信息失败');
    }

    res.send({
      status: 0,
      message: '获取用户信息成功！',
      data: userInfo[0]
    });
  } catch (err) {
    res.eror(err);
  }
};


    
   
exports.upUserInfo =(req,res)=>{
  let value =  req.body
 
  fs.readFile('./middlewares/dao.json', (error, content) => {
    if (error) {
      console.error('Error reading dao.json:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    const data = JSON.parse(content);
    const sql = data.upUserinfo;
  
    let vla = [value.avatar_data ,value.image_path ,value.age, value.gender, value.text_content, value.id]
    // 执行数据库查询
    db.query(sql,vla,(error, results) => {
      if (error) {
        console.error('Error executing database query:', error);
        res.status(500).json({ error: 'Internal server error' });
      } else {
        
        res.send({
          status: 0,
          message: '修改成功！',
         
        });
      }
    });
  });
}
    
  



  exports.addResses = (req, res) => {
    
    const { user_id, detailed_address, street, city, state, zip_code } = req.body;
   
    const sql = `
    INSERT INTO addresses (user_id, detailed_address, street, city, state, zip_code)
    VALUES (?,?,?,?,?,?);
    `;

    db.query(sql, [user_id, detailed_address, street, city, state, zip_code], (err, results) => {
        if (err) return res.eror(err);

        // 查询刚插入的地址
        const getAddressSql = `
        SELECT * FROM addresses
        WHERE user_id = ?
        `;

        db.query(getAddressSql, results.insertId, (err, address) => {
            if (err) return res.error(err);

            res.send({
                status: 0,
                message: "地址添加成功",
                data: address[0]
            });
        });
    });
};

exports.upaddResses = (req, res) => {
    const { id, user_id, detailed_address, street, city, state, zip_code } = req.body;

    const sql = `
    UPDATE addresses
    SET user_id = ?, detailed_address = ?, street = ?, city = ?, state = ?, zip_code = ?
    WHERE id = ?;
    `;

    db.query(sql, [user_id,detailed_address, street, city, state, zip_code, id], (err, results) => {
        if (err) return res.error(err);

        // 查询更新后的地址
        const getAddressSql = `
        SELECT * FROM addresses
        WHERE id = ?
        `;

        db.query(getAddressSql, id, (err, address) => {
            if (err) return res.eror(err);

            res.send({
                status: 0,
                message: "地址更新成功",
                data: address[0]
            });
        });
    });
};


exports.getaddResses = (req, res) => {
  
  const { user_id } = req.query;
 
  const sql = `
  SELECT * FROM addresses
  WHERE user_id = ?;
  `;

  db.query(sql, user_id, (err, addresses) => {
      if (err) return res.eror(err);

      res.send({
          status: 0,
          message: "成功获取地址列表",
          data: addresses
      });
  });
};
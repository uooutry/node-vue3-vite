


const { JsonWebTokenError } = require('jsonwebtoken');
const db = require('../../config/db_config')
const passwordUtils = require('../../services/servicesures')
const jwt = require("jsonwebtoken")
const fs = require('fs');
const {TimeFormatter} = require('../../utils/schema')
// 读取并执行 SQL 文件
const filePath = '../middlewares'; // 文件路径

const config = require('../../config/config')
// 注册处理函数
exports.regUser = (req, res) => {
    // 获取客户端提交服务器的客户信息
    const userinfo = req.body;
    if (!userinfo.account || !userinfo.password) {
      return res.send({ status: 1, message: '用户名或密码不合法' });
    }
  
    const userSql = 'SELECT * FROM users WHERE account = ?';
    const values = [userinfo.account];
    db.query(userSql, values, (err, results) => {
      if (err) {
        return res.eror(err)
      }
  
      if (results.length > 0) {
        // return res.send({ status: 1, message: '用户被占用' });
        return  res.eror("用户名被占用，请更换其他用户名！")
      }
  
      // 调用bcrypt对密码进行加密
      const plainPassword = userinfo.password;
      const hashedPasswordFromDB = '...'; // 假设从数据库中获取的哈希密码
  
      const { hashedPassword, isPasswordMatched, error } = passwordUtils.encryptAndComparePassword(
        plainPassword,
        hashedPasswordFromDB
      );
  
      if (error) {
        res.eror(error)
        // 处理错误情况
      } else {
        // console.log('密码加密后:', hashedPassword);
        // console.log('密码匹配结果:', isPasswordMatched);
  
        if (hashedPassword) {
          // 插入新用户语句
          const pushsql = 'INSERT INTO users SET ?';
  
          // 调用db.query()执行sql语句
          db.query(pushsql, {name:userinfo.name, account: userinfo.account,email:userinfo.email,phone:userinfo.phone, password: hashedPassword , permission:userinfo.permission}, (err, results) => {
            // 判断sql语句是否执行
          
            if (err) {
            //   return res.send({ status: 1, message: err.message });
            res.eror(err)
            }
            // 判断影响行数是否为1
            if (results.affectedRows !== 1) {
            //   return res.send({ status: 1, message: '用户注册失败，请重新输入' });
            res.eror('用户注册失败，请重新输入')
            } 
            const userId = results.insertId;
            if (userId) {
              // 获取刚插入的用户id
              
              
              // 将用户id插入到userinfo表中
              const userinfoSql = 'INSERT INTO user_info (user_id) VALUES (?)';
              db.query(userinfoSql, [userId], (err, userinfoResults) => {
                if (err) {
                  // 处理userinfo表插入错误
                  res.eror(err)
                } else {
                  // 返回注册成功信息
                  res.send({ status: 0, message: '注册成功' });
                }
              });
            }
          });
        }
      }
    });
  };
// 登录处理函数
exports.login = (req,res) => {
  let userinfo = req.body

  const loginSql = 'select * from users where account=?'
  let value = [userinfo.account]
 
  db.query(loginSql,value,function(err,results){
    if (err) return res.eror(err)

    if (results.length !==1) return res.eror('登录失败！')

    // TODO:判断密码是否正确
   // 调用bcrypt对密码进行解密
   const plainPassword = userinfo.password;
   const hashedPasswordFromDB = results[0].password; // 假设从数据库中获取的哈希密码
  
   const { hashedPassword, isPasswordMatched, error } = passwordUtils.encryptAndComparePassword(
     plainPassword,
     hashedPasswordFromDB
   );
    // console.log(isPasswordMatched);
   if (!isPasswordMatched) {
    res.eror(error)
    // 处理错误情况
  }else{
    // TODO:在服务器端生成Token的字符串
    const user = {...results[0],password:''}
    // res.send('log Ok')
    const tokenStr = jwt.sign(user,config.jwtSecretKey,{expiresIn:config.expiresIn})
    
    const datas = {user, token: 'Bearer ' + tokenStr}
    // 调用res.send()讲token响应给客户端
    res.send({
     
      status:0,
      message:'登录成功！',
      data:datas
    })
  }

  })
    
};

//修改密码
exports.upPwd=(req, res) => {
  const userId = req.body.id; // 获取 URL 中的 :userId 参数
  const value = req.body; // 获取请求体中的数据

  const upuserSql = 'select * from users where id=?'
  db.query(upuserSql,[userId],(err, results)=>{
    if (err) return eror(err)

    if (results.length !==1) return res.eror('修改失败')
   
    const plainPassword = value.newpassword;

    const hashedPasswordFromDB = results[0].password; // 假设从数据库中获取的哈希密码
  
    const { hashedPassword, isPasswordMatched, error } = passwordUtils.encryptAndComparePassword(
      plainPassword,
      hashedPasswordFromDB
    );
    if (isPasswordMatched) {
      return res.eror('新密码与旧密码重复')
    }else{
      const sql = 'UPDATE users SET password = ? WHERE id = ?';
      const params = [hashedPassword, userId]; // 假设你有定义 userId
     
      db.query(sql, params, (error, results) => {
        if (error) {
         
          res.status(500).json({ error: '数据库更新出错' });
        } else {
          res.send({ status:0 , message: '用户密码已成功更新' });
        }
      });
    }
  })

  
};

exports.getUser = (req, res) => {
  // 异步读取 dao.json 文件内容
  fs.readFile('./middlewares/dao.json', (error, content) => {
    if (error) {
      console.error('Error reading dao.json:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    const data = JSON.parse(content);
    const sql = data.getUser;
  

    // 执行数据库查询
    db.query(sql, (error, results) => {
      if (error) {
        console.error('Error executing database query:', error);
        res.status(500).json({ error: 'Internal server error' });
      } else {
        const formatter = new TimeFormatter(results, "created_at", 'updated_at'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
        const formattedArr = formatter.format();
        res.send({
          status: 0,
          message: '获取用户信息成功！',
          data: formattedArr
        });
      }
    });
  });
};


exports.getauthUser = (req, res) => {
  let {user_id }= req.query
 
  // 异步读取 dao.json 文件内容
  fs.readFile('./middlewares/dao.json', (error, content) => {
    if (error) {
      console.error('Error reading dao.json:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    const data = JSON.parse(content);
    const sql = data.getauthUser;
  

    // 执行数据库查询
    db.query(sql, [user_id],(error, results) => {
      if (error) {
        console.error('Error executing database query:', error);
        res.status(500).json({ error: 'Internal server error' });
      } else {
       
        res.send({
          status: 0,
          message: '获取用户信息成功！',
          data: results
        });
      }
    });
  });
};


exports.deletUser = (req,res) =>{
  
  let valuse = req.body.id
 
  fs.readFile('./middlewares/dao.json', (error, content) => {
    if (error) {
      console.error('Error reading dao.json:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    const data = JSON.parse(content);
    const sql = data.delectUser;


    // 执行数据库查询
    db.query(sql,[valuse],(error, results) => {
      if (error) {
        console.error('Error executing database query:', error);
        res.status(500).json({ error: 'Internal server error' });
      } else {
        
        res.send({
          status: 0,
          message: '删除成功！',
         
        });
      }
    });
  });
}

exports.upUser =(req,res)=>{
  let value =  req.body
 
  fs.readFile('./middlewares/dao.json', (error, content) => {
    if (error) {
      console.error('Error reading dao.json:', error);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    const data = JSON.parse(content);
    const sql = data.upUser;
 
    let vla = [value.name,value.phone,value.email,value.id]
    // 执行数据库查询
    db.query(sql,vla,(error, results) => {
      if (error) {
        console.error('Error executing database query:', error);
        res.status(500).json({ error: 'Internal server error' });
      } else {
        
        res.send({
          status: 0,
          message: '修改成功！',
         
        });
      }
    });
  });
}


exports.upauthUser = (req,res) =>{
  const {autr_val,user_id} = req.body

  const sql = `UPDATE users SET permission =${autr_val} WHERE id = ${user_id};`
  db.query(sql,(error, results) => {
    if (error) {
      res.send({
        status:1,
        message:'修改失败'
      })
    }
    res.send({
      status:0,
      message:'修改成功'
    })


  })

}
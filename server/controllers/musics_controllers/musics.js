const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const {formatupDate} = require('../../utils/schema')
const {TimeFormatter} = require('../../utils/schema')
exports.addMusics = (req, res) => {
  // 从前端传来的请求中获取音乐信息
 
  const { title, artist, album, genre, duration,audiopath,mvpaht,user_id,user_name ,cover_url} = req.body;

  // 将音乐信息和音乐信息插入数据库
  const sql = 'INSERT INTO musics (title, artist, album, genre, duration, file_path, mv_path,user_id,user_name,cover_url) VALUES (?, ?, ?, ?, ?, ?, ?,?,?,?)';
  const values = [title, artist, album, genre, duration ,audiopath, mvpaht ,user_id,user_name,cover_url];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('音乐信息插入数据库失败:', err);
      return res.status(500).json({ error: '音乐信息插入数据库失败' });
    }

    const musicId = result.insertId;
    return res.send({
      status:0,
      message:'添加成功',
      data:result
    })
  });
};

exports.getadminMusics = (req, res) => {
  // 从前端传来的请求中获取音乐信息
  
  // 将音乐信息和音乐信息插入数据库
  const sql = 'select id, title, artist, album, genre, duration, file_path, mv_path,user_id,user_name,play_count,like_count,cover_url,created_at from musics where is_deleted = 0';

  db.query(sql, (err, result) => {
    if (err) {
      console.error('音乐信息读取数据库失败:', err);
      return res.status(500).json({ error: '音乐信息读取数据库失败' });
    }

    const formatter = new TimeFormatter(result, "created_at", 'update_at'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
    const formattedArr = formatter.format();
    return res.send({
      status:0,
      message:'添加成功',
      data:formattedArr 
    })
  });
};


exports.getuserMusics = (req, res) => {
  // 从前端传来的请求中获取音乐信息
 let { user_id } = req.query 
 
  // 将音乐信息和音乐信息插入数据库
  const sql = 'select id, title, artist, album, genre, duration, file_path, mv_path,user_id,user_name,play_count,like_count,cover_url,created_at from musics where is_deleted = 0 and user_id = ?';

  db.query(sql,user_id, (err, result) => {
    if (err) {
      console.error('音乐信息读取数据库失败:', err);
      return res.status(500).json({ error: '音乐信息读取数据库失败' });
    }
    const formatter = new TimeFormatter(result, "created_at", 'update_at'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
    const formattedArr = formatter.format();
    return res.send({
      status:0,
      message:'添加成功',
      data:formattedArr 
    })
  });
};




exports.delectMusics = (req, res) => {
  // 从前端传来的请求中获取音乐信息
  let { id } = req.body
  // 将音乐信息和音乐信息插入数据库
  const sql = 'UPDATE musics SET is_deleted = 1 where id = ?';

  db.query(sql,id , (err, result) => {
    if (err) {
      console.error('音乐信息读取数据库失败:', err);
      return res.status(500).json({ error: '音乐信息读取数据库失败' });
    }
    return res.send({
      status:0,
      message:'删除成功',
      data:result
    })
  });
};



exports.updatatMusics = (req, res) => {
  // 从前端传来的请求中获取音乐信息
  
  let { id, title, artist, album, genre, duration, audiopath, mvpaht, user_id , user_name ,cover_url} = req.body
  // 将音乐信息和音乐信息插入数据库
  const sql = `UPDATE musics SET  title=?, artist=?, album=?, genre=?, duration=?, file_path=?, mv_path=?  ,cover_url = ? where id = ${id} and user_id= ${ user_id} `

  db.query(sql,[title, artist, album, genre, duration, audiopath, mvpaht,cover_url] ,(err, result) => {
    if (err) {
      console.error('音乐信息读取数据库失败:', err);
      return res.status(500).json({ error: '音乐信息读取数据库失败' });
    }
    return res.send({
      status:0,
      message:'修改成功',
      data:result
    })
  });
};

exports.setMusics = (req, res) => {
  let { id, title, user_name, album, genre ,likeuser,userid} = req.query;
  if (likeuser) {
    const userId = req.query.userid; // 用户ID

    // 查询用户收藏的音乐信息
    const sql = 'SELECT * FROM user_likes WHERE user_id = ?';
    const values = [userId];
  
   db.query(sql, values, (error, results) => {
      if (error) {
        console.error('查询用户喜欢的音乐信息时出错：', error);
        res.status(500).json({ error: '查询用户喜欢的音乐信息时出错' });
      } else {
        if (results.length === 0) {
          // 收藏表中没有对应的记录
          res.status(404).json({ message: '用户没有喜欢任何音乐' });
        } else {
          const musicsids = results.map((result) => result.music_id);
          const videosQuery = 'SELECT * FROM videos WHERE id IN (?)';
          const videosValues = [musicsids];
  
         db.query(videosQuery, videosValues, (error, videos) => {
            if (error) {
              console.error('查询音乐信息时出错：', error);
              res.status(500).json({ error: '查询音乐信息时出错' });
            } else {
            
              res.send({
                status:0,
                message:'查询成功',
                data:videos
              })
            }
          });
        }
      }
    });

  }else{
// 构建 SQL 查询语句
let sql = "SELECT * FROM musics WHERE 1=1";
if (id) {
  sql += ` AND id = '${id}'`;
}
if (title) {
  sql += ` AND title = '${title}'`;
}
if (user_name) {
  sql += ` AND user_name = '${user_name}'`;
}
if (album) {
  sql += ` AND album = '${album}'`;
}
if (genre) {
  sql += ` AND genre = '${genre}'`;
}

// 执行查询语句
db.query(sql, (err, results) => {
  if (err) {
    // 处理错误情况
    console.error("Error executing SQL query:", err);
    res.status(500).json({ error: "Internal Server Error" });
  } else {
    // 将查询结果返回给前端
    res.send({
      status:0,
      message:"查询成功",
      data: results
    })
  }
});
  }
  
};





exports.addLikes =  (req, res) => {
  let { id, userid } = req.body;

  // Check if user has already liked the video
  const checkLikedSql = `SELECT * FROM user_likes WHERE user_id = ${userid} AND musics_id = ${id}`;

  db.query(checkLikedSql, (err, result) => {
    if (err) {
      console.error('查询点赞记录失败:', err);
      return res.status(500).json({ error: '查询点赞记录失败' });
    }

    if (result.length > 0) {
      // User has already liked the video, return error response
      return res.send({
        status: 1,
        message: '已点赞过该音乐',
      });
    } else {
      // Insert user like record into user_likes table
      const insertLikeSql = `INSERT INTO user_likes (user_id, musics_id) VALUES (${userid}, ${id})`;

      db.query(insertLikeSql, (err, result) => {
        if (err) {
          console.error('点赞失败:', err);
          return res.status(500).json({ error: '点赞失败' });
        }

        // Update likes count in videos table
        const updateLikesSql = `UPDATE musics SET like_count = like_count + 1 WHERE id = ${id}`;

        db.query(updateLikesSql, (err, result) => {
          if (err) {
            console.error('更新音乐点赞数失败:', err);
            return res.status(500).json({ error: '更新音乐点赞数失败' });
          }

          return res.send({
            status: 0,
            message: '点赞成功',
          });
        });
      });
    }
  });
};


exports.paly = (req,res)=>{
  let {id} = req.body;
  const sql = `UPDATE musics SET play_count = play_count + 1 WHERE id = ${id}`;
  db.query(sql,(err,result)=>{
    if(err){
      console.error('播放失败:',err);
      return res.status(500).json({error:'播放失败'})
    }
    return res.send({
      status:0,
      message:'播放成功',
      data:result
    })
  })
}


exports.getAll = (req, res) => {
  // 获取要查询的关键字
  const keyword = req.query.keyword;

  // 存储查询结果
  let queryResults = [];

  // 获取数据库连接
 

  // 查询音乐表，匹配 title、artist和user_name字段，按与关键字接近程度排序
  const query = `
    SELECT *,
      CASE WHEN title LIKE '%${keyword}%' THEN 1 ELSE 0 END AS title_match,
      CASE WHEN artist LIKE '%${keyword}%' THEN 1 ELSE 0 END AS artist_match,
      CASE WHEN user_name LIKE '%${keyword}%' THEN 1 ELSE 0 END AS creator_match
    FROM musics
    WHERE title LIKE '%${keyword}%'
      OR artist LIKE '%${keyword}%'
      OR user_name LIKE '%${keyword}%'
    ORDER BY 
      CASE WHEN title LIKE '%${keyword}%' THEN 1 ELSE 0 END DESC,
      CASE WHEN artist LIKE '%${keyword}%' THEN 1 ELSE 0 END DESC,
      CASE WHEN user_name LIKE '%${keyword}%' THEN 1 ELSE 0 END DESC`;
  
  // 执行查询
  db.query(query, (error, results, fields) => {
    if (error) {
      throw error;
    }

    // 将查询结果存入结果集
    queryResults = results;

    // 发送结果给客户端进行响应
    res.send({
      status: 0,
      message: '查询成功',
      data: queryResults,
    });
  });
};



exports.addfavorite = (req,res) => {
  const { userid, musicsid } = req.body;
  
  // 查询是否已存在收藏记录
  const checkExistSql = 'SELECT COUNT(*) AS count FROM music_collection WHERE user_id = ? AND music_id = ?';
  const checkExistValues = [userid, musicsid];

  db.query(checkExistSql, checkExistValues, (error, results) => {
    if (error) {
      console.error('查询收藏记录时出错：', error);
      res.status(500).json({ error: '查询收藏记录时出错' });
    } else {
      const count = results[0].count;
      if (count > 0) {
        // 已存在收藏记录
        res.status(400).json({ message: '不能重复收藏' });
      } else {
        // 插入收藏记录
        const insertSql = 'INSERT INTO music_collection (user_id, music_id) VALUES (?, ?)';
        const insertValues = [userid, musicsid];

        db.query(insertSql, insertValues, (error, results) => {
          if (error) {
            console.error('插入收藏记录时出错：', error);
            res.status(500).json({ error: '插入收藏记录时出错' });
          } else {
           

            // 更新音乐收藏数
            const updateSql = 'UPDATE musics SET favorite_count = favorite_count + 1 WHERE id = ?';
            const updateValues = [musicsid];

            db.query(updateSql, updateValues, (error, results) => {
              if (error) {
                console.error('更新音乐收藏数时出错：', error);
                res.status(500).json({ error: '更新音乐收藏数时出错' });
              } else {
          
                res.status(200).json({ message: '收藏成功' });
              }
            });
          }
        });
      }
    }
  });
}




// 获取用户收藏的音乐信息
exports.getUserFavorites = (req, res) => {
  const userId = req.query.userid; // 用户ID

  // 查询用户收藏的音乐信息
  const sql = 'SELECT * FROM music_collection WHERE user_id = ?';
  const values = [userId];
  
 db.query(sql, values, (error, results) => {
    if (error) {
      console.error('查询用户收藏的音乐信息时出错：', error);
      res.status(500).json({ error: '查询用户收藏的音乐信息时出错' });
    } else {
      if (results.length === 0) {
        // 收藏表中没有对应的记录
        res.status(404).json({ message: '用户没有收藏任何音乐' });
      } else {
        const videoIds = results.map((result) => result.music_id);
        const videosQuery = 'SELECT * FROM musics WHERE id IN (?)';
        const videosValues = [videoIds];
        
       db.query(videosQuery, videosValues, (error, videos) => {
          if (error) {
            console.error('查询音乐信息时出错：', error);
            res.status(500).json({ error: '查询音乐信息时出错' });
          } else {
        
            res.send({
              status:0,
              message:'查询成功',
              data:videos
            })
          }
        });
      }
    }
  });
};


exports.getmusicsPlayCount = (req,res)=>{
  let { user_id } = req.query

  const sql = `SELECT genre, play_count,updated_at
  FROM musics
  WHERE user_id = ${user_id};`

  db.query(sql,(error,result)=>{
    if (error) {
     
      return res.status(500).json({error:'查询失败'})
    }
   let datas =  formatupDate(result)
    return res.send({
      status:0,
      message:'查询成功',
      data:datas
    })
  })
}
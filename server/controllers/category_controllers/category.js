const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config')
const {TimeFormatter} = require('../../utils/schema')
exports.getCategory = (req,res)=>{
    try {
        // 获取用户 ID
        fs.readFile('./middlewares/dao.json', (error, content) => {
          if (error) {
            console.error('Error reading dao.json:', error);
            res.status(500).json({ error: 'Internal server error' });
            return;
          }
    
          const data = JSON.parse(content);
          const sql = data.getCategory;
         
          // 执行数据库查询
          // 将图片路径存储到 user_info 表中的 image_path 列中
       
          db.query(sql, (error, results) => {
            if (error) {
              console.error(error);
              return res.status(500).json({ error: '获取失败' });
            } else {
              const formatter = new TimeFormatter(results, "created_at", 'updated_at'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
              const formattedArr = formatter.format();
              res.send({
                status: 0,
                message: '获取成功',
                data:formattedArr
              });
            }
           
          });
        });
    
      } catch (err) {
        console.error(err);
        return res.status(500).json({ error: '获取失败' });
      }
}


exports.getVideoCategory = (req, res) => {
  db.query('SELECT * FROM categories WHERE video = 1', (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).json({ error: '获取失败' });
    } else {
      const formatter = new TimeFormatter(results, "created_at", 'updated_at'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
              const formattedArr = formatter.format();
      res.send({
        status: 0,
        message: '获取成功',
        data: formattedArr 
      });
    }
  });
}

exports.getmusicCategory = (req, res) => {
  db.query('SELECT * FROM categories WHERE music = 1', (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).json({ error: '获取失败' });
    } else {
      res.send({
        status: 0,
        message: '获取成功',
        data: results
      });
    }
  });
}


exports.getproductCategory = (req, res) => {
  db.query('SELECT * FROM categories WHERE product = 1', (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).json({ error: '获取失败' });
    } else {
      res.send({
        status: 0,
        message: '获取成功',
        data: results
      });
    }
  });
}
exports.addCategory = (req, res) => {
  let { valused, category_name } = req.body;

  let field = '';
  let sql = '';
  if (valused === '视频分类') {
    field = 'video';
  } else if (valused === '商品分类') {
    field = 'product';
  } else if (valused === '音乐分类') {
    field = 'music';
  }

  if (field) {
    sql = `INSERT INTO categories (${field}, category_name) VALUES (1, ?)`;
    db.query(sql, category_name, (error, results) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: '添加失败' });
      } else {
        res.send({
          status: 0,
          message: '添加成功',
        });
      }
    });
  } else {
    res.status(400).json({ error: '无效的分类' });
  }
}
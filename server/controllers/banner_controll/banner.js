const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config')
const {TimeFormatter} = require('../../utils/schema')
exports.Banner = (req,res)=>{
    const sql = `select * from banner where id_delected = 0`
    db.query(sql,(err,results)=>{
        if(err) return res.eror(err)
        const formatter = new TimeFormatter(results, "created_at", 'update_time'); // 第一个参数为数组，第二个参数开始为多个时间字段名称
        const formattedArr = formatter.format();
      
        res.send({
            status: 0,
            message:'获取成功',
            data: formattedArr 
        })

    })
}
exports.addBanner = (req, res) => {
  try {
    // 获取用户 ID
    const userId = req.body.user_id; // 假设从请求中获取了用户 ID
    fs.readFile('./middlewares/dao.json', (error, content) => {
      if (error) {
        console.error('Error reading dao.json:', error);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }

      const data = JSON.parse(content);
      const sql = data.addBanner;
  
      // 执行数据库查询
      // 将图片路径存储到 user_info 表中的 image_path 列中
      let { img_url, url } = req.body;
      db.query(sql, [img_url, url], (error, results) => {
        if (error) {
          console.error(error);
          return res.status(500).json({ error: '文件上传失败' });
        } else {
          res.send({
            status: 0,
            message: '成功'
          });
        }
        
      });
    });

  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: '文件上传失败' });
  }
};


exports.delectBanner = (req, res) => {
  try {
    // 获取用户 ID
    const userId = req.body.user_id; // 假设从请求中获取了用户 ID
  
    // 查询用户权限的语句
    const userPermissionSql = `SELECT permission FROM users WHERE id = ?`;

    // 查询用户权限
    db.query(userPermissionSql, userId, (error, userResults) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: '查询用户权限失败' });
      }

      // 检查用户权限
      const userPermission = userResults[0].permission;
      if (userPermission !== 0) {
        return res.status(403).json({ error: '没有权限修改' });
      }
      const id = req.body.id
      // 修改 banner 表的 is_deleted 字段
      const updateBannerSql = `UPDATE banner SET id_delected = 1 WHERE id = ?`;
      db.query(updateBannerSql, id, (updateError, updateResults) => {
        if (updateError) {
          console.error(updateError);
          return res.status(500).json({ error: '删除失败' });
        } else {
          return res.json({
            status: 0,
            message: '删除成功'
          });
        }
      });
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: '更新失败' });
  }
};

exports.upBanner = (req, res) => {
  try {
     
    
      // 获取用户 ID
      const userId = req.body.user_id; // 假设从请求中获取了用户 ID
      fs.readFile('./middlewares/dao.json', (error, content) => {
        if (error) {
          console.error('Error reading dao.json:', error);
          res.status(500).json({ error: 'Internal server error' });
          return;
        }
    
        const data = JSON.parse(content);
        const sql = data.upBanner;
        let val  = req.body
       
        // 执行数据库查询
         // 将图片路径存储到 user_info 表中的 image_path 列中
        db.query(sql,[val.img_url,val.url,val.id],(error, results) => {
          if (error) {
            console.error(error);
            return res.status(500).json({ error: '文件上传失败' });
          } else {
            
           // 返回成功响应
            return res.json({
              status: 0,
              message: '修改成功',
             
            });
          }
        });
      });
     
    } catch (err) {
      console.error(err);
      return res.status(500).json({ error: '文件上传失败' });
    }
};
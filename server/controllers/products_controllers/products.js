const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config')

const {convertImageUrlsToArrays} = require('../../utils/schema')
exports.Products = ( req, res)=>{
    fs.readFile('./middlewares/dao.json', (error, content) => {
        if (error) {
          console.error('Error reading dao.json:', error);
          res.status(500).json({ error: 'Internal server error' });
          return;
        }
    
        const data = JSON.parse(content);
        const sql = data.getProducts;
       const categorysql = "select category_name from categories where is_deleted = 0"
    db.query(sql,categorysql, (err, results) => {
        if (err) return res.error(err);
        let newdata = convertImageUrlsToArrays(results)
        res.send({
          status: 0,
          message: `获取成功`,
          data: newdata
      });
    });
    })
}


exports.addProducts = (req, res) => {
  try {
    // 验证和过滤输入数据
    let val = req.body;
  
    // 向 products 表插入数据
    const insertProductSQL = `
      INSERT INTO products (name, description, price, category, stock, original_price, user_id, user_name, image_urls)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
   
    `;

    // 执行 SQL 查询并处理结果
    db.query(insertProductSQL, [
      val.name, val.description, val.price, val.category, val.stock,
      val.original_price, val.user_id, val.user_name, val.image_urls,
    ], (err, results) => {
      if (err) {
        console.error('Error executing SQL query:', err);
        return res.status(500).json({ err: 'Internal server error' });
      }

      // 返回成功响应
      res.send({
        status: 0,
        message: '添加成功',
        data: results,
      });
    });
  } catch (err) {
    console.error('Error processing request:', err);
    return res.status(500).json({ err: 'Internal server error' });
  }
};


  exports.upProducts = (req, res) => {
    try {
      // 验证和过滤输入数据
      let val = req.body;
    
      // 更新 products 表中符合条件的数据
      const updateProductSQL = `
        UPDATE products
        SET name = ?, description = ?, price = ?, category = ?, stock = ?, original_price = ?, image_urls = ?
        WHERE id = ?
      `;
  
      // 执行 SQL 查询并处理结果
      db.query(updateProductSQL, [
        val.name, val.description, val.price, val.category, val.stock,
        val.original_price, val.image_urls, val.id
      ], (err, results) => {
        if (err) {
          console.error('Error executing SQL query:', err);
          return res.status(500).json({ err: 'Internal server error' });
        }
  
        // 返回成功响应
        res.send({
          status: 0,
          message: '修改成功',
          data: results,
        });
      });
    } catch (err) {
      console.error('Error processing request:', err);
      return res.status(500).json({ err: 'Internal server error' });
    }
  };


  
exports.delectProducts = (req, res) => {
  try {
    // 获取用户 ID
    const userId = req.body.user_id; // 假设从请求中获取了用户 ID
   
    // 查询用户权限的语句
    const userPermissionSql = `SELECT permission FROM users WHERE id = ?`;

    // 查询用户权限
    db.query(userPermissionSql, userId, (error, userResults) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: '查询用户权限失败' });
      }

      // 检查用户权限
      const userPermission = userResults[0].permission;
      if (userPermission !== 0) {
        return res.status(403).json({ error: '没有权限修改' });
      }
      const id = req.body.id
    
      // 修改 banner 表的 is_deleted 字段
      const updateBannerSql = `UPDATE products SET is_deleted = 1 WHERE id = ?`;
      db.query(updateBannerSql, id, (updateError, updateResults) => {
        if (updateError) {
          console.error(updateError);
          return res.status(500).json({ error: '删除失败' });
        } else {
          return res.json({
            status: 0,
            message: '删除成功'
          });
        }
      });
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: '更新失败' });
  }
};


exports.setProducts = (req,res) =>{
  const { id, user_id, category, user_name, name } = req.query; // 获取前端发送的字段
  
  // 构建动态的 SQL 查询语句
  let sql = 'SELECT * FROM products WHERE is_deleted = 0';
  if (id) {
    sql += ` AND id = '${id}'`;
  }
  if (user_id) {
    sql += ` AND user_id = '${user_id}'`;
  }
  if (category) {
    sql += ` AND category = '${category}'`;
  }
  if (user_name) {
    sql += ` AND user_name = '${user_name}'`;
  }
  if (name) {
    sql += ` AND name = '${name}'`;
  }

  // 执行查询操作
  db.query(sql, (err, results) => {
    if (err) {
      console.error('Error querying database:', err);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }

    res.send({
      status: 0,
      message: '查询成功',
      data: results
    });
  });
}

exports.objProducts = (req,res) =>{

  let val = req.body.id
 

  
  // // 构建动态的 SQL 查询语句
  let sql = `SELECT * FROM products WHERE is_deleted = 0 and id = ${val}`;

  // }

  // // 执行查询操作
  db.query(sql, (err, results) => {
    if (err) {
      console.error('Error querying database:', err);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }
    let newarr = convertImageUrlsToArrays(results)
    
    res.send({
      status: 0,
      message: '查询成功',
      data: newarr
    });
  });
}


exports.leikeProducts = (req,res)=>{
  let { id, userid } = req.body;

  // Check if user has already liked the video
  const checkLikedSql = `SELECT * FROM user_likes WHERE user_id = ${userid} AND products_id = ${id}`;

  db.query(checkLikedSql, (err, result) => {
    if (err) {
      console.error('查询点赞记录失败:', err);
      return res.status(500).json({ error: '查询点赞记录失败' });
    }

    if (result.length > 0) {
      // User has already liked the video, return error response
      return res.send({
        status: 1,
        message: '已点赞过该音乐',
      });
    } else {
      // Insert user like record into user_likes table
      const insertLikeSql = `INSERT INTO user_likes (user_id, products_id) VALUES (${userid}, ${id})`;

      db.query(insertLikeSql, (err, result) => {
        if (err) {
          console.error('点赞失败:', err);
          return res.status(500).json({ error: '点赞失败' });
        }

        // Update likes count in videos table
        const updateLikesSql = `UPDATE products SET likes = likes + 1 WHERE id = ${id}`;

        db.query(updateLikesSql, (err, result) => {
          if (err) {
            console.error('更新音乐点赞数失败:', err);
            return res.status(500).json({ error: '更新音乐点赞数失败' });
          }

          return res.send({
            status: 0,
            message: '点赞成功',
          });
        });
      });
    }
  });
}


exports.leikeProducts = (req,res)=>{
  let { id, userid } = req.body;

  // Check if user has already liked the video
  const checkLikedSql = `SELECT * FROM user_likes WHERE user_id = ${userid} AND products_id = ${id}`;

  db.query(checkLikedSql, (err, result) => {
    if (err) {
      console.error('查询点赞记录失败:', err);
      return res.status(500).json({ error: '查询点赞记录失败' });
    }

    if (result.length > 0) {
      // User has already liked the video, return error response
      return res.send({
        status: 1,
        message: '已点赞过该音乐',
      });
    } else {
      // Insert user like record into user_likes table
      const insertLikeSql = `INSERT INTO user_likes (user_id, products_id) VALUES (${userid}, ${id})`;

      db.query(insertLikeSql, (err, result) => {
        if (err) {
          console.error('点赞失败:', err);
          return res.status(500).json({ error: '点赞失败' });
        }

        // Update likes count in videos table
        const updateLikesSql = `UPDATE products SET likes = likes + 1 WHERE id = ${id}`;

        db.query(updateLikesSql, (err, result) => {
          if (err) {
            console.error('更新音乐点赞数失败:', err);
            return res.status(500).json({ error: '更新音乐点赞数失败' });
          }

          return res.send({
            status: 0,
            message: '点赞成功',
          });
        });
      });
    }
  });
}

exports.faVorites = (req,res)=>{
  const { userid, productsid } = req.body;
  
  // 查询是否已存在收藏记录
  const checkExistSql = 'SELECT COUNT(*) AS count FROM product_collections WHERE user_id = ? AND product_id = ?';
  const checkExistValues = [userid, productsid];

  db.query(checkExistSql, checkExistValues, (error, results) => {
    if (error) {
      console.error('查询收藏记录时出错：', error);
      res.status(500).json({ error: '查询收藏记录时出错' });
    } else {
      const count = results[0].count;
      if (count > 0) {
        // 已存在收藏记录
        res.status(400).json({ message: '不能重复收藏' });
      } else {
        // 插入收藏记录
        const insertSql = 'INSERT INTO product_collections (user_id, product_id) VALUES (?, ?)';
        const insertValues = [userid, productsid];

        db.query(insertSql, insertValues, (error, results) => {
          if (error) {
            console.error('插入收藏记录时出错：', error);
            res.status(500).json({ error: '插入收藏记录时出错' });
          } else {
     

            // 更新视频收藏数
            const updateSql = 'UPDATE products SET favorites = favorites + 1 WHERE id = ?';
            const updateValues = [productsid];

            db.query(updateSql, updateValues, (error, results) => {
              if (error) {
                console.error('更新视频收藏数时出错：', error);
                res.status(500).json({ error: '更新视频收藏数时出错' });
              } else {
          
                res.status(200).json({ message: '收藏成功' });
              }
            });
          }
        });
      }
    }
  });
}



// 获取用户收藏的视频信息
exports.getUserFavorites = (req, res) => {
  const userId = req.query.userid; // 用户ID
 
  // 查询用户收藏的视频信息
  const sql = 'SELECT * FROM product_collections WHERE user_id = ?';
  const values = [userId];

 db.query(sql, values, (error, results) => {
    if (error) {
      console.error('查询用户收藏的视频信息时出错：', error);
      res.status(500).json({ error: '查询用户收藏的视频信息时出错' });
    } else {
      if (results.length === 0) {
        // 收藏表中没有对应的记录
        res.status(404).json({ message: '用户没有收藏任何视频' });
      } else {
        const products = results.map((result) => result.product_id);
        const productsQuery = 'SELECT * FROM products WHERE id IN (?)';
        const productsValues = [products];

       db.query(productsQuery, productsValues, (error, product) => {
          if (error) {
            console.error('查询视频信息时出错：', error);
            res.status(500).json({ error: '查询视频信息时出错' });
          } else {
            
            let newdata = convertImageUrlsToArrays(product)
            res.send({
              status:0,
              message:'查询成功',
              data:newdata
            })
          }
        });
      }
    }
  });
};



exports.getAll = (req, res) => {
// 获取要查询的关键字
const keyword = req.query.keyword;
if (keyword==='') {
  res.send({
    status: 1,
    message: '请输入关键字',
  });
}else{
  let queryResults = [];

  // 获取数据库连接
  
  
  // 查询音乐表，匹配 name、category和user_name字段，按与关键字接近程度排序
  const query = `
    SELECT *,
      CASE WHEN name LIKE '%${keyword}%' THEN 1 ELSE 0 END AS name_match,
      CASE WHEN category LIKE '%${keyword}%' THEN 1 ELSE 0 END AS category_match,
      CASE WHEN user_name LIKE '%${keyword}%' THEN 1 ELSE 0 END AS user_name_match
    FROM products
    WHERE name LIKE '%${keyword}%'
      OR category LIKE '%${keyword}%'
      OR user_name LIKE '%${keyword}%'
   `;
  
  // 执行查询
  db.query(query, (error, results, fields) => {
    if (error) {
      throw error;
    }
  
    // 将查询结果存入结果集
    queryResults = results;
  
    // 发送结果给客户端进行响应
    let newdata = convertImageUrlsToArrays(queryResults)
    res.send({
      status: 0,
      message: '查询成功',
      data: newdata,
    });
  });
}



}

const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const util = require('util');
const {TimeFormatter,convertImageUrlsToArrays} = require('../../utils/schema')

// 将db.query函数转换为Promise形式
const queryPromise = util.promisify(db.query).bind(db);

exports.addOrders = async (req, res) => {
  try {
    const data = req.body;


    // 获取最新的订单号
    const getOrderNumberSql = `SELECT MAX(order_number) AS max_order_number FROM ordersamount`;
    const orderNumberResult = await queryPromise(getOrderNumberSql);
    const latestOrderNumber = orderNumberResult[0].max_order_number;
    const newOrderNumber = latestOrderNumber ? latestOrderNumber + 1 : 1;

    // 根据 user_id 获取地址信息
    const getAddressSql = `SELECT * FROM addresses WHERE user_id = ?`;
    const addressResult = await queryPromise(getAddressSql, [data.user_id]);
    const address = addressResult[0];

    // 拼接地址字段
    const fullAddress = `${address.state} ${address.city} ${address.street}  ${address.detailed_address}`;

    // 插入数据到 ordersamount 表
    const total_amount = parseInt(data.merchant_quantity) * parseInt(data.total_amount);
    const insertSql = `INSERT INTO ordersamount (order_number, merchant_quantity, total_amount, user_id, merchant_id, product_id, merchant_name, phone_number, address,payment_method) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    const insertResult = await queryPromise(insertSql, [newOrderNumber, data.merchant_quantity, total_amount, data.user_id, data.merchant_id, data.product_id, data.merchant_name, data.phone_number, fullAddress,data.payment_method]);

    // 将地址信息插入到订单数据中
    const result = {
      ...insertResult,
      address: fullAddress,
      order_number: newOrderNumber,
    };

    // 返回成功响应
    return res.send({
      status:0,
      data:result,
      message:'购买成功'
    })
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Failed to add orders' });
  }
};

exports.usergetOrders = async (req, res) => {
  try {
    const orderuserId = req.query.user_id;
    
    const ordersAmountSql = `SELECT * FROM ordersamount WHERE user_id = ?`;
    const ordersAmountResult = await queryPromise(ordersAmountSql, [orderuserId]);

    if (ordersAmountResult.length === 0) {
      return res.status(404).json({ error: "Data not found" });
    }

    const combinedData = [];
  
    console.log(ordersAmountResult.length);
    for (let i = 0; i < ordersAmountResult.length; i++) {
      const merchantId = ordersAmountResult[i].merchant_id;
      const productId = ordersAmountResult[i].product_id;

      const productsSql = `SELECT * FROM products WHERE id = ?`;
      const productsResult = await queryPromise(productsSql, [merchantId]);

      if (productsResult.length === 0) {
        return res.status(404).json({ error: "Data not found" });
      }

      const combinedOrders = {
        ...ordersAmountResult[i],
        ...productsResult[0]
      };
      combinedData.push(combinedOrders)

    }
   
   let newcombinedData = new TimeFormatter(combinedData,"orders_time","updated_at","created_at")
   const formattedArr = newcombinedData.format();

   let newdata = convertImageUrlsToArrays(formattedArr)
    res.send({
      status: 0,
      message: '获取成功',
      data: newdata
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};


exports.adminGetOrders = async (req, res) => {

  try {
    const orderuserId = req.query.user_id
    
    const ordersAmountSql = `SELECT * FROM ordersamount WHERE product_id = ?`;
    const ordersAmountResult = await queryPromise(ordersAmountSql, [orderuserId]);
    
    if (ordersAmountResult.length === 0) {
      return res.status(404).json({ error: "Data not found" });
    }
  
    const combinedData = [];
    const addedOrderNumbers = [];
  
    for (let i = 0; i < ordersAmountResult.length; i++) {
      const merchantId = ordersAmountResult[i].merchant_id;
      const productId = ordersAmountResult[i].product_id;
  
      const productsSql = `SELECT * FROM products WHERE user_id = ? AND id = ?`;
      const productsResult = await queryPromise(productsSql, [productId, merchantId]);
  
      if (productsResult.length === 0) {
        return res.status(404).json({ error: "Data not found" });
      }
  
      const merchantQuantity = ordersAmountResult[i].merchant_quantity;
  
      for (let j = 0; j < merchantQuantity; j++) {
        const orderNumber = ordersAmountResult[i].order_number;
        
        if (addedOrderNumbers.includes(orderNumber)) {
          continue;
        }
  
        const combinedOrder = {
          ...ordersAmountResult[i],
          ...productsResult[0]
        };
        
        combinedData.push(combinedOrder);
        addedOrderNumbers.push(orderNumber);
      }
    }
  
    res.send({
      status: 0,
      message: '获取成功',
      data: combinedData
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};


exports.Delivergoods = (req, res) => {
  let val = req.body;
  

  const product_id = val.product_id;
  const order_number = val.order_number;
  const merchant_id = val.merchant_id;
  const merchant_quantity = parseInt(val.merchant_quantity);

  // 更新ordersamount表中对应订单的pending_shipment字段为1
  const updatePendingShipmentQuery = `UPDATE ordersamount SET pending_shipment = 1 WHERE product_id = ? AND order_number = ?`;
  db.query(updatePendingShipmentQuery, [product_id, order_number], (err, result) => {
    if (err) {
      console.log(err);
      res.status(500).send("无法修改");
      return;
    }

    // 根据merchant_id在products表中查找对应的商品
    const findProductQuery = `SELECT * FROM products WHERE id = ?`;
    db.query(findProductQuery, merchant_id, (err, rows) => {
      if (err) {
        console.log(err);
        res.status(500).send("未找到该商品");
        return;
      }

      if (rows.length === 0) {
        res.status(404).send("没有数据");
        return;
      }

      const product = rows[0];
      const currentStock = product.stock;
      const currentSales = product.sales;

      // 更新products表中对应商品的库存和销量
      const newStock = currentStock - merchant_quantity;
      const newSales = currentSales + merchant_quantity;

      const updateProductQuery = `UPDATE products SET stock = ?, sales = ? WHERE id = ?`;
      db.query(updateProductQuery, [newStock, newSales, merchant_id], (err, result) => {
        if (err) {
          console.log(err);
          res.status(500).send("无法发货");
          return;
        }

        res.send({
          status:1,
          message:'发货成功'
        })
      });
    });
  });
};

exports.reCeipt = (req,res) =>{
  let {order_number} = req.body;
  
  const sql  = "UPDATE ordersamount SET shipped = 1  WHERE order_number = ?;"
  db.query(sql ,order_number , (err, result) => {
    if (err) {
      console.log(err);
      res.status(500).send("无法收货");
      return;
    }

    res.send({
      status:1,
      message:'收货成功'
    })
  });
}


exports.setDeliver = (req, res) => {
  const { id } = req.query;

  let sql = 'SELECT * FROM ordersamount WHERE order_number = ?';
  let params = [id];
  
  db.query(sql, params, (err, ordersResults) => {
    if (err) {
      console.error('Error querying ordersamount table:', err);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }
  
    const ordersData = ordersResults;
  
    if (ordersData.length === 0) {
      res.send({
        status: 0,
        message: '查询成功',
        data: []
      });
      return;
    }
  
    const merchantId = ordersData[0].merchant_id;
  
    let productsSql = 'SELECT * FROM products WHERE id = ?';
    let productsParams = [merchantId];
  
    db.query(productsSql, productsParams, (err, productsResults) => {
      if (err) {
        console.error('Error querying products table:', err);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const productsData = productsResults;
  
      const responseData = {
        ...ordersData[0],
        ...productsData[0]
      };
  
      res.send({
        status: 0,
        message: '查询成功',
        data: responseData
      });
    });
  });
};


exports.setuserDeliver = (req,res)=>{
  const { orderid ,user_id} = req.query;
  console.log(req.query);
  let sql = 'SELECT * FROM ordersamount WHERE order_number = ? and user_id = ?';
  let params = [orderid,user_id];
  console.log(params);
  db.query(sql, params, (err, ordersResults) => {
    if (err) {
      console.error('Error querying ordersamount table:', err);
      res.status(500).json({ error: 'Internal server error' });
      return;
    }
  
    const ordersData = ordersResults;
  
    if (ordersData.length === 0) {
      res.send({
        status: 0,
        message: '查询成功',
        data: []
      });
      return;
    }
  
    const merchantId = ordersData[0].merchant_id;
  
    let productsSql = 'SELECT * FROM products WHERE id = ?';
    let productsParams = [merchantId];
  
    db.query(productsSql, productsParams, (err, productsResults) => {
      if (err) {
        console.error('Error querying products table:', err);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const productsData = productsResults;
  
      const responseData = {
        ...ordersData[0],
        ...productsData[0]
      };
  
      res.send({
        status: 0,
        message: '查询成功',
        data: responseData
      });
    });
  });
}

const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config');
const {TimeFormatter} = require('../../utils/schema')
exports.addCart = (req, res) => {
    const { user_id, product_id, quantity, amount } = req.body; // 假设请求体中包含用户ID、商品ID、数量和金额
 
    // 在此处插入逻辑，将购物车项添加到数据库中的购物车表
    const query = `INSERT INTO shopping_cart (user_id, product_id, quantity, amount) VALUES (${user_id}, ${product_id}, ${quantity}, ${amount})`;
  
    db.query(query, (err, result) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: '添加购物车失败' });
      } else {
        // 添加成功后，查询商品数据并响应给前端
        const getProductQuery = `SELECT * FROM products WHERE id = ${product_id}`;
        db.query(getProductQuery, (err, productResult) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: '无法获取产品数据' });
          } else {
            const product = productResult[0]; // 假设查询结果只有一行数据
            const cartItem = {
              cart_id: result.cart_id,
              user_id,
              product,
              quantity,
              amount
            };
            // res.status(200).json(cartItem);
            res.send({
                status: 0,
                message: '添加购物！',
                // data: cartItem
              });
          }
        });
      }
    });
  };


  exports.getCartDataByUserId = (req, res) => {
    const user_id = req.query.user_id; // 假设URL中通过参数传递了用户ID
    // 查询购物车数据
    
    const getCartQuery = `SELECT * FROM shopping_cart WHERE user_id = ${user_id} and is_deleted = 0 ` ;
    db.query(getCartQuery, (err, cartResult) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: '暂无数据' });
      } else {
        // 购物车数据
        const cartData = cartResult.map(item => ({
          cart_id: item.cart_id,
          user_id: item.user_id,
          product_id: item.product_id,
          quantity: item.quantity,
          amount: item.amount,
          isSelected:item.isSelected
        }));
  
        // 获取商品数据
        const productIds = cartData.map(item => item.product_id).join(','); // 构造商品ID字符串
        if (productIds == '') {
          return
        }
        const getProductQuery = `SELECT * FROM products WHERE id IN (${productIds})`;
        db.query(getProductQuery, (err, productResult) => {
          if (err) {
            console.error(err);
            res.status(500).json({ message: '无法获取商品数据' });
          } else {
            // 商品数据
            const productData = productResult.reduce((acc, cur) => {
                const image_urls = cur.image_urls.split(','); // 将字符串转换为数组
                const updatedProduct = { ...cur, image_urls }; // 更新包含转换后的数组的新商品对象
                acc[cur.id] = updatedProduct;
                return acc;
              }, {});
  
            // 将商品数据与购物车数据进行关联
            const mergedData = cartData.map(item => ({
              cart_id: item.cart_id,
              user_id: item.user_id,
              product: productData[item.product_id],
              quantity: item.quantity,
              amount: item.amount,
              isSelected:item.isSelected
            }));
  
            res.status(200).json(mergedData);
          }
        });
      }
    });
  };

  exports.delectCartDataByUserId= (req,res)=>{
   
    
    let {cart_id ,user_id} = req.body
    
    const sql = `UPDATE shopping_cart SET is_deleted = 1 WHERE cart_id = ${cart_id} and user_id = ${user_id}`;
    db.query(sql, (err, result) => {
        if (err) {
          console.error(err);
          res.status(500).json({ message: '无法获取产品数据' });
        } 
        res.send({
            status:0,
            message:'删除成功'
        })
      });
  }
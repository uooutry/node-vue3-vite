const fs = require('fs');
const path = require('path');
const db = require('../../config/db_config')
exports.upLoadimg =(req, res) => {
 
  try {
    if (!req.file) {
      return res.status(400).json({ error: 'No file uploaded' });
    }
  
    // 获取用户 ID
    const userId = req.body.user_id; // 假设从请求中获取了用户 ID
    
    // 将文件保存到指定目录
    const targetPath = path.join(__dirname, '..', '..', 'uploads', req.file.filename);
    // 将上传文件移动到目标路径
    fs.renameSync(req.file.path, targetPath);
    
    fs.readFile('./middlewares/dao.json', (error, content) => {
      if (error) {
        console.error('Error reading dao.json:', error);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const data = JSON.parse(content);
      const sql = data.upuserImage;
      const currentUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
     
      const regex = /\/user\/upload/g;
      const newUrl = `${currentUrl.replace(regex, '/uploads?')}filename=${req.file.filename}`;
   
      // 执行数据库查询
       // 将图片路径存储到 user_info 表中的 image_path 列中
       // 返回成功响应
       return res.json({
        status: 0,
        message: '文件成功上传',
        filename: req.file.filename,
        data:newUrl
      });
    });
   
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: '文件上传失败' });
  }
};


exports.updateAvatar = (req,res)=>{

  try {
    if (!req.file) {
      return res.status(400).json({ error: 'No file uploaded' });
    }
  
    // 获取用户 ID
    const userId = req.body.user_id; // 假设从请求中获取了用户 ID
    
    // 将文件保存到指定目录
    const targetPath = path.join(__dirname, '..', '..', 'uploads', req.file.filename);
    // 将上传文件移动到目标路径
    fs.renameSync(req.file.path, targetPath);
    
    fs.readFile('./middlewares/dao.json', (error, content) => {
      if (error) {
        console.error('Error reading dao.json:', error);
        res.status(500).json({ error: 'Internal server error' });
        return;
      }
  
      const data = JSON.parse(content);
      const sql = data.upAvatar;
      const currentUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
      
      const regex = /\/user\/update\/avatar/;
      const newUrl = `${currentUrl.replace(regex, '/uploads?')}filename=${req.file.filename}`;
    
      // 执行数据库查询
       // 将图片路径存储到 user_info 表中的 image_path 列中
      db.query(sql,[newUrl, userId],(error, results) => {
        if (error) {
          console.error(error);
          return res.status(500).json({ error: '文件上传失败' });
        } else {
          
         // 返回成功响应
          return res.json({
            status: 0,
            message: '文件成功上传',
            filename: req.file.filename
          });
        }
      });
    });
   
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: '文件上传失败' });
  }

}


exports.delectUplads = (req,res) =>{
 
  const filePath = path.join(__dirname, '..', '..', 'uploads', req.body.fileName);;
  fs.unlinkSync(filePath);
  
}
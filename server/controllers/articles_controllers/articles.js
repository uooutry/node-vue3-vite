const db = require('../../config/db_config')
const {TimeFormatter} = require('../../utils/schema')
exports.arTicles = (req, res) => {
    const vla = req.query;
    if (vla.id) {
      const sql = `SELECT a.id, a.title, a.author, a.content, a.publish_date, a.category, a.user_id, a.image_path, a.is_deleted, a.created_at, a.updated_at
      FROM articles as a
      WHERE a.is_deleted = 0 and id = ?`;
  
      db.query(sql, [vla.id], (err, results) => {
        if (err) return res.eror(err);
        res.send({
          status: 0,
          message: '获取文章数据成功!',
          data: results,
        });
      });
    } else {
      const sql = `SELECT a.id, a.title, a.author, a.content, a.publish_date, a.category, a.user_id, a.image_path, a.is_deleted, a.created_at, a.updated_at
      FROM articles as a
      WHERE a.is_deleted = 0`;
  
      db.query(sql, (err, results) => {
        if (err) return res.eror(err);

        const formatter = new TimeFormatter(results, "created_at", "updated_at","publish_date"); // 第一个参数为数组，第二个参数开始为多个时间字段名称
        const formattedArr = formatter.format();
        res.send({
          status: 0,
          message: '获取文章数据成功!',
          data:formattedArr ,
        });
      });
    }
  };

exports.addArticles = (req, res) => {
    const values = req.body;
    console.log(values);
    const sql = 'INSERT INTO articles SET ?';
    db.query(sql, values, (err, results) => {
        if (err) {
            return res.eror(err);
        }

        if (results.affectedRows !== 1) {
            return res.error('新增文章失败');
        }
        res.send({
            status: 0,
            message:'新增文章成功！'
              
        });
        
    });
    
};

exports.delectArticles= (req, res) => {
   
    const sql = `UPDATE articles SET is_deleted=1 WHERE id=?`
    db.query(sql, req.body.id, (err, results) => {
        if (err) {
            return res.error(err);
        }

        if (results.affectedRows !== 1) {
            return res.eror('删除失败');
        }

       
        res.send({
            status: 0,
            message:'删除文章成功！'
              
        });
        
    });
   
}


// 通过分类查询

exports.quryArticles=(req,res)=>{
    console.log( req.query.category);
   
  
    const sql = `SELECT *
    FROM articles
    WHERE category = ? AND is_deleted = 0;
    `
    db.query(sql,req.query.category,(err,results)=>{
        if (err) return res.eror(err)
        let fonsDate = formatDate(results)
        res.send({
            status:0,
            message:`分类为${req.query.category}获取成功`,
            data:fonsDate
        })
    })
}

exports.inputArticles = (req, res) => {
    console.log(req.query.keyword);

    const keyword = `%${req.query.keyword}%`; // 加入通配符 % 来进行模糊匹配

    const sql = `
    SELECT *
    FROM articles
    WHERE (title LIKE ? OR content LIKE ? OR author LIKE ? ) AND is_deleted = 0; 
    `;

    db.query(sql, [keyword, keyword,keyword], (err, results) => {
        if (err) return res.eror(err);
        let fonsDate = formatDate(results)
        res.send({
            status: 0,
            message: `关键字获取成功`,
            data:  fonsDate 
        });
    });
};


exports.updataArticles = (req, res) => {
    const articleId = req.params.id; // 假设文章ID作为URL参数传递
    const newdata = req.body; // 假设新的标题从请求体中获取

    const sql = `
    UPDATE articles
    SET ?
    WHERE id = ?;
    `;

    db.query(sql, [newdata, newdata.id], (err, results) => {
        if (err) return res.error(err);
       
        res.send({
            status: 0,
            message: `文章标题更新成功`,
            data: results[0]
        });
    });
};


exports.getArticlesnew = (req, res) => {
    const articleId = req.params.id; // 假设文章ID作为URL参数传递
    const newdata = req.body; // 假设新的标题从请求体中获取

    const sql = `
    select articles
    SET ?
    WHERE id = ?;
    `;

    db.query(sql, [newdata, newdata.id], (err, results) => {
        if (err) return res.error(err);

        res.send({
            status: 0,
            message: `文章标题更新成功`,
            data: results[0]
        });
    });
};
const express = require('express')
const multer = require('multer');
const path = require('path');
const router = express.Router()
const {checkToken} = require('../utils/schema')

const user_controll = require('../controllers/user_controll/user.js')
let userinfo_controll = require('../controllers/user_controll/userinfo.js')
const articles_controll = require('../controllers/articles_controllers/articles.js') 
const banner_controll = require('../controllers/banner_controll/banner.js')
const upload_controll = require('../controllers/upload_controllers/uploads.js');
const products_controll = require('../controllers/products_controllers/products.js')
const category_controll = require('../controllers/category_controllers/category.js')
const comments_controll = require('../controllers/comments_controllers/comments.js');
const cart_controll = require('../controllers/cart_controllers/cart.js')
const orders_controll = require('../controllers/orders_controllers/orders.js')

const musics_controllers = require('../controllers/musics_controllers/musics.js')

const videos_controllers = require('../controllers/videos_controllers/videos.js')

const message_controllers = require('../controllers/message_controllers/meassage.js')

const  recommendations = require('../controllers/commonality')
// 注册
router.post('/user/reguser',user_controll.regUser)

// 登录
router.post('/user/login',user_controll.login)


router.get("/user",user_controll.getUser)
router.post("/push/upauthUser",user_controll.upauthUser)
//获取权限
router.get("/get/userauth",user_controll.getauthUser)
router.post("/user",user_controll.upUser)

// 删除用户
router.post("/user/deletuser",user_controll.deletUser)

// 获取用户信息
router.get('/user/userinfo',userinfo_controll.getUserInfo)


// 更新用户信息
router.post('/user/userinfo',userinfo_controll.upUserInfo)

// 修改账号信息
router.post('/user/userpwd',user_controll.upPwd)




// 地址路径
router.post('/user/addresses',userinfo_controll.addResses)
// 更改地址
router.post('/user/upaddresses',userinfo_controll.upaddResses)

// 获取地址
router.get('/user/addresses',userinfo_controll.getaddResses)



// 添加文章
router.post('/add/articles',articles_controll.addArticles)

//删除文章
router.post('/delect/articles',articles_controll.delectArticles)

// 更新文章
router.post('/update/articles',articles_controll.updataArticles)

// 获取文章
router.get('/articles',articles_controll.arTicles)


// 获取分类文章结果
router.get('/qury/articles',articles_controll.quryArticles)

// input框获取查询对应的数据
router.get('/qury/articles',articles_controll.inputArticles)

// 获取轮播图
router.get('/banner',banner_controll.Banner)
router.post('/delct/banner',banner_controll.delectBanner)
router.post('/updata/banner',banner_controll.upBanner)
router.post('/banner',banner_controll.addBanner)

//商品
router.get("/products",products_controll.Products)
router.post("/products",products_controll.addProducts)
router.post("/update/products",products_controll.upProducts)
router.post("/delct/products",products_controll.delectProducts)
router.get("/set/products",products_controll.setProducts)
router.post("/obj/products",products_controll.objProducts)
router.post("/leike/products",products_controll.leikeProducts)
router.post("/leike/products",products_controll.leikeProducts)
router.post('/add/productsfavorites',products_controll.faVorites)
router.get('/get/productsfavorites',products_controll.getUserFavorites)
router.get('/set/getproductsAll',products_controll.getAll)
//分类
router.get("/category",category_controll.getCategory)
//商品分类
router.get("/get/productscategory",category_controll.getproductCategory)
//音乐分类
router.get("/get/musicscategory",category_controll.getmusicCategory)
//视频分类
router.get("/get/videosategory",category_controll.getVideoCategory )
router.post("/category",category_controll.addCategory)

//评论
router.post("/get/comments",comments_controll.Comments)
router.post("/add/comments",comments_controll.AddComments)
router.post('/incrementLikes/comments',comments_controll.incrementLikes)
router.get('/get/commentsObj',comments_controll.getCommentsAll)
router.post('/delect/Comment',comments_controll.deleteComment)
//回复评论
router.post('/add/replies',comments_controll.addReplies)
router.get('/get/repliesObj',comments_controll.getReplies)
// 购物车

router.post('/add/cart',cart_controll.addCart)
router.get('/get/cart',cart_controll.getCartDataByUserId)
router.post('/delect/cart',cart_controll.delectCartDataByUserId)

//订单
router.post('/add/orders',orders_controll.addOrders)
router.get('/get/orders',orders_controll.usergetOrders)
router.get('/get/adminorders',orders_controll.adminGetOrders)
router.post('/push/Delivergoods',orders_controll.Delivergoods)
router.get('/set/Deliver',orders_controll.setDeliver)
router.get('/set/userDeliver',orders_controll.setuserDeliver)
router.post('/push/receipt',orders_controll.reCeipt)

// const { uploadMiddleware, bodyParserMiddleware } = require('../middlewares/uploadMiddleware');
//音乐
router.post('/add/musics',musics_controllers.addMusics)
router.get("/get/musics",musics_controllers.getadminMusics)
router.get("/getuser/musics",musics_controllers.getuserMusics)
router.post("/delct/musics",musics_controllers.delectMusics)
router.post("/updata/musics",musics_controllers.updatatMusics)
router.get("/set/musics",musics_controllers.setMusics)
router.post("/push/addLikes",musics_controllers.addLikes)
router.post("/push/addpaly",musics_controllers.paly)
router.get('/set/getAll',musics_controllers.getAll)
router.post('/push/favorites',musics_controllers.addfavorite)
router.get("/get/userfavorite",musics_controllers.getUserFavorites)

router.get("/get/getmusicsPlayCount",musics_controllers.getmusicsPlayCount)

//视频
router.post('/add/videos',videos_controllers.addvideos)
router.get('/get/adminvideos',videos_controllers.getadminvideos)
router.post('/delect/adminvideos',videos_controllers.delectvideos)
router.post('/updat/videos',videos_controllers.uptvideos)
router.get('/set/videos',videos_controllers.setVideos)
router.get('/obj/videos',videos_controllers.getObject)
router.post('/add/likevideos',videos_controllers.likes)
router.post('/add/subTableslikes',videos_controllers.subTableslikes)
router.post('/add/favorites',videos_controllers.favorites)
router.get('/get/userfavorites',videos_controllers.getUserFavorites)
router.post('/add/addplay',videos_controllers.addplay)
router.get('/set/videosgetAll',videos_controllers.getAll)
router.post('/add/episodes',videos_controllers.addEpisodes)
router.get('/get/episodes',videos_controllers.getEpisodes)
router.get('/get/episodesObj',videos_controllers.getEpisodesObj)
router.get('/get/getallviePlayCount',videos_controllers.getallviePlayCount) //播放之和

//消息
router.get('/get/messages',message_controllers.getMessage)



// 设置存储引擎和上传路径
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads\/');
    },
    filename: function (req, file, cb) {
      const extension = path.extname(file.originalname);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
      cb(null, uniqueSuffix + extension);
    }
  });
  
  // 创建multer实例
  const upload = multer({ storage: storage });
  // 添加轮播图

//上传用户图片
router.post('/dMiddleware',upload.single('image'),upload_controll.upLoadimg)
router.post('/user/upload',upload.single('image'),upload_controll.upLoadimg)
// GET 请求接口，用来获取上传的图片
router.get('/uploads', (req, res) => {
    const filename = req.query.filename;
    const filePath = path.join(__dirname, '..', 'uploads', filename);
    res.sendFile(filePath);
  });
  //更换头像的路由

router.post('/delect/uplads',upload_controll.delectUplads)

module.exports = router
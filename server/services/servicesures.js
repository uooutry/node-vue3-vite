const bcrypt = require('bcryptjs');
// 用户加密方法
exports.encryptAndComparePassword = (plainPassword, hashedPasswordFromDB) => {
  try {
    // 加密密码
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const hashedPassword = bcrypt.hashSync(plainPassword, salt);

    // 验证密码是否匹配
    const isPasswordMatched = bcrypt.compareSync(plainPassword, hashedPasswordFromDB);
   
    // 返回密码加密和验证结果
    return {
      hashedPassword,
      isPasswordMatched
    };
  } catch (error) {
    // 处理错误
    console.error('密码加密和验证出错:', error);

    // 返回错误信息
    return {
      error: '密码加密和验证出错'
    };
  }
};






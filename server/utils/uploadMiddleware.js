const multer = require('multer');
const bodyParser = require('body-parser');

// 设置存储上传文件的路径和文件名
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/uploads');
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop());
  }
});

// 创建multer上传中间件
const upload = multer({ storage: storage });

// 导出中间件和解析表单数据对象
module.exports = {
  uploadMiddleware: upload.fields([
    { name: 'musicFile', maxCount: 1 },
    { name: 'videoFile', maxCount: 1 }
  ]),
  bodyParserMiddleware: bodyParser.urlencoded({ extended: true })
};
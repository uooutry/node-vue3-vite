const moment = require('moment');

function convertDateFormat(dateStr) {
  try {
    // 使用 moment.js 将提供的日期字符串转换为指定的格式
    const convertedDate = moment(dateStr, 'YYYY-MM-DDTHH:mm:ss.SSSZ').format('YYYY-MM-DD');
    
    return convertedDate;
  } catch (error) {
    return '日期格式不正确';
  }
}


function formatDate(arr) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    const obj = arr[i];
    const { created_at, ...rest } = obj;
    const date = new Date(created_at);
    const formattedDate = date.toLocaleString();
    result.push({ created_at: formattedDate, ...rest });
  }
  return result;
}

function formatupDate(arr) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    const obj = arr[i];
    const { updated_at, ...rest } = obj;
    const date = new Date(updated_at);
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // 注意月份是从0开始的，所以需要加1
    const day = date.getDate();
    const formattedDate = `${year}:${month}:${day}`;
    result.push({ updated_at: formattedDate, ...rest });
  }
  return result;
}
class TimeFormatter {
  constructor(arr, ...fields) {
    this.arr = arr;
    this.fields = fields;
  }

  format() {
    const result = [];
    for (let i = 0; i < this.arr.length; i++) {
      const obj = this.arr[i];
      const updatedObj = {};
      
      // 遍历每个时间字段
      for (let j = 0; j < this.fields.length; j++) {
        const field = this.fields[j];

        // 如果该字段有值，则进行格式化
        if (obj[field]) {
          const date = new Date(obj[field]);
          const year = date.getFullYear();
          const month = date.getMonth() + 1; // 注意月份是从0开始的，所以需要加1
          const day = date.getDate();
          const hours = date.getHours();
          const minutes = date.getMinutes();
          const seconds = date.getSeconds();
          const formattedDate = `${year}-${month}-${day}`;
          const formattedTime = `${hours}:${minutes}:${seconds}`;
          updatedObj[field] = `${formattedDate} ${formattedTime}`;
        }
      }
      result.push({ ...obj, ...updatedObj });
    }
    return result;
  }
}

function convertImageUrlsToArrays(arr) {
  return arr.map(obj => {
    if (obj.hasOwnProperty('image_urls') && obj.image_urls !== null && obj.image_urls !== '') {
      obj.image_urls = obj.image_urls.split(',');
    }
    return obj;
  });
}



// 中间件来检查 token
const checkToken = (req, res, next) => {
  const token = req.headers.authorization;
  
  if (token) {
    // 在这里进行 token 验证的逻辑
    // 检查 token 的有效性，比如解码、验证签名等
    // 如果 token 有效，则继续处理请求
    // 如果 token 无效，则返回相应的错误响应
    if (isValidToken(token)) {
      // token 有效
      next();
    } else {
      // token 无效，返回错误响应
      res.status(401).json({ error: 'Invalid token' });
    }
  } else {
    // 如果请求头中没有携带 token，则返回错误响应
    res.status(401).json(
      { error: 'Missing token',
        status:1,
        message:'请登录在添加'
    }
      );
  }
};


module.exports = {
    convertDateFormat,
    formatDate,
    formatupDate,
    TimeFormatter,
    convertImageUrlsToArrays,
    checkToken
  };
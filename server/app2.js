// 引入依赖包
const speech = require('@google-cloud/speech').v1p1beta1;
const fs = require('fs');

// 创建一个新的客户端
const client = new speech.SpeechClient();

// 定义音频转文字的函数
async function audioToText(audioFile) {
  const file = fs.readFileSync(audioFile);
  const audioBytes = file.toString('base64');

  // 构建音频配置对象
  const audio = {
    content: audioBytes,
  };

  // 构建识别配置对象
  const config = {
    encoding: 'MP3', // 音频编码格式（根据实际情况选择）
    sampleRateHertz: 44100, // 音频采样率（根据实际情况进行设置）
    languageCode: 'en-US', // 语言代码（根据实际情况选择）
  };

  // 构建请求对象
  const request = {
    audio: audio,
    config: config,
  };

  // 发送请求并等待响应
  const [response] = await client.recognize(request);

  // 解析响应结果，提取文字
  const transcription = response.results
    .map(result => result.alternatives[0].transcript)
    .join('\n');

  return transcription;
}

// 调用音频转文字函数，并传入音频文件路径
audioToText("./uploads/1697630188265-829478271.mp3")
  .then(transcription => {
    console.log('转换结果：');
    console.log(transcription);
  })
  .catch(err => {
    console.error('发生错误：', err);
  });
const mysql = require('mysql');

// 创建数据库连接
const connection = mysql.createConnection({
  host: 'localhost',     // 数据库主机地址
  port: 3306,            // 数据库端口号
  user: 'root',  // 数据库用户名
  password: 'root',  // 数据库密码
  database: 'my_db'  // 数据库名称
});

// 连接数据库
connection.connect((err) => {
  if (err) {
    console.error('数据库连接失败:', err);
    return;
  }
  console.log('数据库连接成功');
});

// 当连接断开时触发的事件
connection.on('end', () => {
  console.log('数据库连接断开');
});

// 当应用程序关闭时，关闭数据库连接
process.on('SIGINT', () => {
  connection.end(() => {
    console.log('数据库连接已关闭');
    process.exit(0);
  });
});


module.exports = connection
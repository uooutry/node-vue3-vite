const express = require('express');
const cors  = require('cors')
const logger = require('./utils/logger');

const app = express();

// 身份令牌解析
var { expressjwt: jwt } = require("express-jwt");
const config = require('./config/config')
app.use(cors())

// 配置解析表单数据的中间件 解析application/x-www-form-urlencoded
app.use(express.urlencoded({extended:false}))

app.use((seq,res,next)=>{

  // status默认值为1 ，表示失败的情况
  // err 的值，可能是一个错误的对象，也可能是一个错误的字符串
  res.eror = function(err,status = 1){
    res.send({
      status,
      message: err instanceof Error ? err.message : err,
    })
  }
  next()
})



let userRouter = require('./router/router_api')

// 日志中间件
app.use('/api', (req, res, next) => {
  logger.info(`[${req.method}] ${req.url}`);
  next();
});

app.use('/api',userRouter)

app.use((err,req,res,next)=>{
  // 身份认证失败后的错误
  if (err.name === 'UnauthorizedError') {
    return res.eror('身份认证失败')
  }
})

const port = 3005;
app.listen(port, () => {
  console.log(`Server running on port ${'http://127.0.0.1:'+port}`);
});